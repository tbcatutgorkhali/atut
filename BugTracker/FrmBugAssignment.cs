﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmBugAssignment : Form
    {
        BugAssignmentBL BugAssignmentBl = null;
        BugBL bugBl = null;
        LoginBL loginBl = null;
        public FrmBugAssignment()
        {
            InitializeComponent();
            BugAssignmentBl = new BugAssignmentBL();
            bugBl = new BugBL();
            loginBl = new LoginBL();
            GetBugAssignmentList();
            LoadUserDropDown();
            LoadBugDropDown();
        }
        public static int BugId;
        private static FrmBugAssignment BugAssignment = null;
        public static FrmBugAssignment Instance()
        {
            if (BugAssignment == null)
            {
                BugAssignment = new FrmBugAssignment();
            }
            return BugAssignment;
        }

        private void FrmBugAssignment_FormClosed(object sender, FormClosedEventArgs e)
        {
            BugAssignment = null;
        }

        private void GetBugAssignmentList()
        {
            var BugAssignmentList = BugAssignmentBl.List();
            if (BugAssignmentList.Any())
            {
                dataGridViewBugAssignment.DataSource = null;
                dataGridViewBugAssignment.DataSource = BugAssignmentList.ToList();
            }
        }

        private void btnSaveBugAssignment_Click(object sender, EventArgs e)
        {
            if (cmbBugId.SelectedIndex < 0 || cmbAssginTo.SelectedIndex < 0)
            {
                MessageBox.Show("Provide all requied fields.");
                return;
            }
            var model = new BugAssignmentModel();
            int id = 0;
            int userId = 0;
            int projectId = 0;
            int.TryParse(txtBugAssignmentId.Text.Trim().ToString(), out id);
            int.TryParse(cmbBugId.SelectedValue.ToString(), out projectId);
            int.TryParse(cmbAssginTo.SelectedValue.ToString(), out userId);
            model.BugAssignmentId = id;
            model.BugId = projectId;
            model.AssignedToId = userId;
            
            if (id == 0)
            {
                var createResult = BugAssignmentBl.Create(model);
                if (createResult)
                {
                    GetBugAssignmentList();
                    this.Close();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Assignment already exist. Assignment Failed");
                }
            }
            else
            {
                var editResult = BugAssignmentBl.Edit(model);
                if (editResult)
                {
                    GetBugAssignmentList();
                    this.Close();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
        private void ResetForm()
        {
            txtBugAssignmentId.Text = "";
            cmbAssginTo.SelectedIndex = -1;
            cmbBugId.SelectedIndex = -1;
            btnDelete.Visible = false;
        }
        private void LoadBugDropDown()
        {
            var list = bugBl.BugList();
            cmbBugId.DataSource = list.ToList();
            cmbBugId.DisplayMember = "Summary";
            cmbBugId.ValueMember = "BugId";
        }
        private void LoadUserDropDown()
        {
            var list = loginBl.List();
            cmbAssginTo.DataSource = list.ToList();
            cmbAssginTo.DisplayMember = "Username";
            cmbAssginTo.ValueMember = "LoginId";
        }
        private void dataGridViewBugAssignment_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            var model = new BugAssignmentModel();
            model.BugAssignmentId = int.Parse(dataGridViewBugAssignment.Rows[index].Cells[0].Value.ToString());
            model.BugId = int.Parse(dataGridViewBugAssignment.Rows[index].Cells[1].Value.ToString());
            model.AssignedToId = int.Parse(dataGridViewBugAssignment.Rows[index].Cells[2].Value.ToString());
            txtBugAssignmentId.Text = model.BugAssignmentId.ToString();
            cmbBugId.SelectedValue = model.BugId;
            cmbAssginTo.SelectedValue = model.AssignedToId;
            btnDelete.Visible = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int BugAssignmentId = int.Parse(txtBugAssignmentId.Text.Trim());
            var deleteResult = BugAssignmentBl.Delete(BugAssignmentId);
            if (deleteResult)
            {
                GetBugAssignmentList();
                ResetForm();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }

        private void FrmBugAssignment_Load(object sender, EventArgs e)
        {
            if (BugId > 0)
            {
                cmbBugId.SelectedValue = BugId;
                cmbBugId.Enabled = false;
            }
        }
    }
}
