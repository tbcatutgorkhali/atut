﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmProjecAccess : Form
    {
        ProjecAccessBL ProjecAccessBl = null;
        ProjectBL projectBl = null;
        LoginBL loginBl = null;
        public FrmProjecAccess()
        {
            InitializeComponent();
            ProjecAccessBl = new ProjecAccessBL();
            projectBl = new ProjectBL();
            loginBl = new LoginBL();
            GetProjecAccessList();
            LoadUserDropDown();
            LoadProjectDropDown();
        }
        private static FrmProjecAccess ProjecAccess = null;
        public static FrmProjecAccess Instance()
        {
            if (ProjecAccess == null)
            {
                ProjecAccess = new FrmProjecAccess();
            }
            return ProjecAccess;
        }

        private void FrmProjecAccess_FormClosed(object sender, FormClosedEventArgs e)
        {
            ProjecAccess = null;
        }

        private void GetProjecAccessList()
        {
            var ProjecAccessList = ProjecAccessBl.List();
            if (ProjecAccessList.Any())
            {
                dataGridViewProjecAccess.DataSource = null;
                dataGridViewProjecAccess.DataSource = ProjecAccessList.ToList();
            }
        }

        private void btnSaveProjecAccess_Click(object sender, EventArgs e)
        {
            if (cmbProjectTitle.SelectedIndex < 0)
            {
                return;
            }
            var model = new ProjecAccessModel();
            int id = 0;
            int userId = 0;
            int projectId = 0;
            int.TryParse(txtProjecAccessId.Text.Trim().ToString(), out id);
            int.TryParse(cmbProjectTitle.SelectedValue.ToString(), out projectId);
            int.TryParse(cmbAssginTo.SelectedValue.ToString(), out userId);
            model.ProjectAccessId = id;
            model.ProjectId = projectId;
            model.UserId = userId;
            if (id == 0)
            {
                var createResult = ProjecAccessBl.Create(model);
                if (createResult)
                {
                    GetProjecAccessList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                var editResult = ProjecAccessBl.Edit(model);
                if (editResult)
                {
                    GetProjecAccessList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
        private void ResetForm()
        {
            txtProjecAccessId.Text = "";
            cmbAssginTo.SelectedIndex = -1;
            cmbProjectTitle.SelectedIndex = -1;
            btnDelete.Visible = false;
        }
        private void LoadProjectDropDown()
        {
            var list = projectBl.List();
            cmbProjectTitle.DataSource = list.ToList();
            cmbProjectTitle.DisplayMember = "Title";
            cmbProjectTitle.ValueMember = "ProjectId";
        }
        private void LoadUserDropDown()
        {
            var list = loginBl.List();
            cmbAssginTo.DataSource = list.ToList();
            cmbAssginTo.DisplayMember = "Username";
            cmbAssginTo.ValueMember = "LoginId";
        }
        private void dataGridViewProjecAccess_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            var model = new ProjecAccessModel();
            model.ProjectAccessId = int.Parse(dataGridViewProjecAccess.Rows[index].Cells[0].Value.ToString());
            model.ProjectId = int.Parse(dataGridViewProjecAccess.Rows[index].Cells[1].Value.ToString());
            model.UserId = int.Parse(dataGridViewProjecAccess.Rows[index].Cells[2].Value.ToString());
            txtProjecAccessId.Text = model.ProjectAccessId.ToString();
            cmbProjectTitle.SelectedValue = model.ProjectId;
            cmbAssginTo.SelectedValue = model.UserId;
            btnDelete.Visible = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int ProjecAccessId = int.Parse(txtProjecAccessId.Text.Trim());
            var deleteResult = ProjecAccessBl.Delete(ProjecAccessId);
            if (deleteResult)
            {
                GetProjecAccessList();
                ResetForm();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }

        private void FrmProjecAccess_Load(object sender, EventArgs e)
        {

        }
    }
}
