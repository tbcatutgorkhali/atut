﻿namespace BugTracker
{
    partial class FrmBugDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBugDetails));
            this.label1 = new System.Windows.Forms.Label();
            this.txtBugId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSummary = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.cmbProductId = new System.Windows.Forms.ComboBox();
            this.cmbComponentId = new System.Windows.Forms.ComboBox();
            this.cmbVersionId = new System.Windows.Forms.ComboBox();
            this.cmbPlatformId = new System.Windows.Forms.ComboBox();
            this.PicBoxAttachment = new System.Windows.Forms.PictureBox();
            this.cmbSeverity = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAttachmentText = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnBugUpdate = new System.Windows.Forms.Button();
            this.fctb = new FastColoredTextBoxNS.FastColoredTextBox();
            this.btnAddComment = new System.Windows.Forms.Button();
            this.dgvComments = new System.Windows.Forms.DataGridView();
            this.btnSetStatus = new System.Windows.Forms.Button();
            this.btnAssignment = new System.Windows.Forms.Button();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxAttachment)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fctb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComments)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(8, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "BugId";
            this.label1.Visible = false;
            // 
            // txtBugId
            // 
            this.txtBugId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtBugId.Location = new System.Drawing.Point(64, 27);
            this.txtBugId.Name = "txtBugId";
            this.txtBugId.Size = new System.Drawing.Size(238, 29);
            this.txtBugId.TabIndex = 1;
            this.txtBugId.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label3.Location = new System.Drawing.Point(11, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 21);
            this.label3.TabIndex = 3;
            this.label3.Text = "Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label4.Location = new System.Drawing.Point(38, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 21);
            this.label4.TabIndex = 4;
            this.label4.Text = "Version";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label7.Location = new System.Drawing.Point(11, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 21);
            this.label7.TabIndex = 7;
            this.label7.Text = "Component";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label8.Location = new System.Drawing.Point(372, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 21);
            this.label8.TabIndex = 8;
            this.label8.Text = "Platform";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label9.Location = new System.Drawing.Point(364, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 21);
            this.label9.TabIndex = 9;
            this.label9.Text = "Summary";
            // 
            // txtSummary
            // 
            this.txtSummary.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSummary.Location = new System.Drawing.Point(448, 113);
            this.txtSummary.Multiline = true;
            this.txtSummary.Name = "txtSummary";
            this.txtSummary.Size = new System.Drawing.Size(252, 109);
            this.txtSummary.TabIndex = 6;
            // 
            // txtDescription
            // 
            this.txtDescription.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtDescription.Location = new System.Drawing.Point(106, 148);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(336, 74);
            this.txtDescription.TabIndex = 7;
            // 
            // cmbProductId
            // 
            this.cmbProductId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProductId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbProductId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbProductId.Location = new System.Drawing.Point(106, 43);
            this.cmbProductId.Name = "cmbProductId";
            this.cmbProductId.Size = new System.Drawing.Size(252, 29);
            this.cmbProductId.TabIndex = 1;
            // 
            // cmbComponentId
            // 
            this.cmbComponentId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbComponentId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbComponentId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbComponentId.Location = new System.Drawing.Point(106, 113);
            this.cmbComponentId.Name = "cmbComponentId";
            this.cmbComponentId.Size = new System.Drawing.Size(252, 29);
            this.cmbComponentId.TabIndex = 5;
            // 
            // cmbVersionId
            // 
            this.cmbVersionId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVersionId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbVersionId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbVersionId.Location = new System.Drawing.Point(106, 78);
            this.cmbVersionId.Name = "cmbVersionId";
            this.cmbVersionId.Size = new System.Drawing.Size(252, 29);
            this.cmbVersionId.TabIndex = 3;
            // 
            // cmbPlatformId
            // 
            this.cmbPlatformId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPlatformId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPlatformId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbPlatformId.Location = new System.Drawing.Point(448, 43);
            this.cmbPlatformId.Name = "cmbPlatformId";
            this.cmbPlatformId.Size = new System.Drawing.Size(252, 29);
            this.cmbPlatformId.TabIndex = 2;
            // 
            // PicBoxAttachment
            // 
            this.PicBoxAttachment.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.PicBoxAttachment.Location = new System.Drawing.Point(706, 43);
            this.PicBoxAttachment.Name = "PicBoxAttachment";
            this.PicBoxAttachment.Size = new System.Drawing.Size(378, 179);
            this.PicBoxAttachment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBoxAttachment.TabIndex = 27;
            this.PicBoxAttachment.TabStop = false;
            this.PicBoxAttachment.Click += new System.EventHandler(this.PicBoxAttachment_Click);
            // 
            // cmbSeverity
            // 
            this.cmbSeverity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeverity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSeverity.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbSeverity.Location = new System.Drawing.Point(448, 78);
            this.cmbSeverity.Name = "cmbSeverity";
            this.cmbSeverity.Size = new System.Drawing.Size(252, 29);
            this.cmbSeverity.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label2.Location = new System.Drawing.Point(372, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 21);
            this.label2.TabIndex = 29;
            this.label2.Text = "Severity";
            // 
            // txtAttachmentText
            // 
            this.txtAttachmentText.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtAttachmentText.Location = new System.Drawing.Point(106, 228);
            this.txtAttachmentText.Multiline = true;
            this.txtAttachmentText.Name = "txtAttachmentText";
            this.txtAttachmentText.Size = new System.Drawing.Size(259, 271);
            this.txtAttachmentText.TabIndex = 30;
            this.txtAttachmentText.Visible = false;
            this.txtAttachmentText.TextChanged += new System.EventHandler(this.txtAttachmentText_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label10.Location = new System.Drawing.Point(6, 231);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 21);
            this.label10.TabIndex = 31;
            this.label10.Text = "Attach Code";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label5.Location = new System.Drawing.Point(38, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 21);
            this.label5.TabIndex = 32;
            this.label5.Text = "Project";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 711);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1090, 22);
            this.statusStrip1.TabIndex = 37;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // btnBugUpdate
            // 
            this.btnBugUpdate.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnBugUpdate.Location = new System.Drawing.Point(106, 505);
            this.btnBugUpdate.Name = "btnBugUpdate";
            this.btnBugUpdate.Size = new System.Drawing.Size(102, 29);
            this.btnBugUpdate.TabIndex = 38;
            this.btnBugUpdate.Text = "Update";
            this.btnBugUpdate.UseVisualStyleBackColor = true;
            this.btnBugUpdate.Click += new System.EventHandler(this.btnBugUpdate_Click);
            // 
            // fctb
            // 
            this.fctb.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.fctb.AutoIndentCharsPatterns = "^\\s*[\\w\\.]+(\\s\\w+)?\\s*(?<range>=)\\s*(?<range>[^;]+);\r\n^\\s*(case|default)\\s*[^:]*(" +
    "?<range>:)\\s*(?<range>[^;]+);";
            this.fctb.AutoIndentExistingLines = false;
            this.fctb.AutoScrollMinSize = new System.Drawing.Size(32, 15);
            this.fctb.BackBrush = null;
            this.fctb.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fctb.CharHeight = 15;
            this.fctb.CharWidth = 7;
            this.fctb.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fctb.DelayedEventsInterval = 200;
            this.fctb.DelayedTextChangedInterval = 500;
            this.fctb.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.fctb.Font = new System.Drawing.Font("Consolas", 9.75F);
            this.fctb.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.fctb.IsReplaceMode = false;
            this.fctb.Location = new System.Drawing.Point(106, 228);
            this.fctb.Name = "fctb";
            this.fctb.Paddings = new System.Windows.Forms.Padding(0);
            this.fctb.PreferredLineWidth = 80;
            this.fctb.ReservedCountOfLineNumberChars = 2;
            this.fctb.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.fctb.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("fctb.ServiceColors")));
            this.fctb.Size = new System.Drawing.Size(978, 271);
            this.fctb.TabIndex = 39;
            this.fctb.Zoom = 100;
            this.fctb.TextChanged += new System.EventHandler<FastColoredTextBoxNS.TextChangedEventArgs>(this.fctb_TextChanged);
            // 
            // btnAddComment
            // 
            this.btnAddComment.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAddComment.Location = new System.Drawing.Point(214, 505);
            this.btnAddComment.Name = "btnAddComment";
            this.btnAddComment.Size = new System.Drawing.Size(151, 29);
            this.btnAddComment.TabIndex = 40;
            this.btnAddComment.Text = "Add Comment";
            this.btnAddComment.UseVisualStyleBackColor = true;
            this.btnAddComment.Click += new System.EventHandler(this.btnAddComment_Click);
            // 
            // dgvComments
            // 
            this.dgvComments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvComments.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvComments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvComments.Location = new System.Drawing.Point(106, 540);
            this.dgvComments.Name = "dgvComments";
            this.dgvComments.Size = new System.Drawing.Size(978, 149);
            this.dgvComments.TabIndex = 41;
            // 
            // btnSetStatus
            // 
            this.btnSetStatus.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnSetStatus.Location = new System.Drawing.Point(687, 505);
            this.btnSetStatus.Name = "btnSetStatus";
            this.btnSetStatus.Size = new System.Drawing.Size(102, 29);
            this.btnSetStatus.TabIndex = 45;
            this.btnSetStatus.Text = "Set";
            this.btnSetStatus.UseVisualStyleBackColor = true;
            this.btnSetStatus.Click += new System.EventHandler(this.btnSetStatus_Click);
            // 
            // btnAssignment
            // 
            this.btnAssignment.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAssignment.Location = new System.Drawing.Point(795, 505);
            this.btnAssignment.Name = "btnAssignment";
            this.btnAssignment.Size = new System.Drawing.Size(120, 29);
            this.btnAssignment.TabIndex = 44;
            this.btnAssignment.Text = "Assign";
            this.btnAssignment.UseVisualStyleBackColor = true;
            this.btnAssignment.Click += new System.EventHandler(this.btnAssignment_Click);
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbStatus.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbStatus.Location = new System.Drawing.Point(429, 505);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(252, 29);
            this.cmbStatus.TabIndex = 42;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label6.Location = new System.Drawing.Point(371, 509);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 21);
            this.label6.TabIndex = 43;
            this.label6.Text = "Status";
            // 
            // FrmBugDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 733);
            this.Controls.Add(this.btnSetStatus);
            this.Controls.Add(this.btnAssignment);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgvComments);
            this.Controls.Add(this.btnAddComment);
            this.Controls.Add(this.fctb);
            this.Controls.Add(this.btnBugUpdate);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAttachmentText);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbSeverity);
            this.Controls.Add(this.PicBoxAttachment);
            this.Controls.Add(this.cmbPlatformId);
            this.Controls.Add(this.cmbVersionId);
            this.Controls.Add(this.cmbComponentId);
            this.Controls.Add(this.cmbProductId);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtSummary);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBugId);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "FrmBugDetails";
            this.Text = "Bug Details";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmBugDetails_FormClosed);
            this.Load += new System.EventHandler(this.FrmBugDetailsAdmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxAttachment)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fctb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComments)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBugId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSummary;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.ComboBox cmbProductId;
        private System.Windows.Forms.ComboBox cmbComponentId;
        private System.Windows.Forms.ComboBox cmbVersionId;
        private System.Windows.Forms.ComboBox cmbPlatformId;
        private System.Windows.Forms.PictureBox PicBoxAttachment;
        private System.Windows.Forms.ComboBox cmbSeverity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAttachmentText;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnBugUpdate;
        private FastColoredTextBoxNS.FastColoredTextBox fctb;
        private System.Windows.Forms.Button btnAddComment;
        private System.Windows.Forms.DataGridView dgvComments;
        private System.Windows.Forms.Button btnSetStatus;
        private System.Windows.Forms.Button btnAssignment;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label label6;
    }
}