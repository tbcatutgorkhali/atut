﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmUser : Form
    {
        LoginBL LoginBl = null;
        //UserTypeBL userTypeBl = null;
        public FrmUser()
        {
            InitializeComponent();
            LoginBl = new LoginBL();
            //userTypeBl = new UserTypeBL();
            GetLoginList();
            LoadUserTypeDropDown();
        }
        private static FrmUser Login = null;
        public static FrmUser Instance()
        {
            if (Login == null)
            {
                Login = new FrmUser();
            }
            return Login;
        }

        private void FrmUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            Login = null;
        }

        private void GetLoginList()
        {
            var LoginList = LoginBl.List();
            if (LoginList.Any())
            {
                dataGridViewUser.DataSource = null;
                dataGridViewUser.DataSource = LoginList.ToList();
            }
        }
        private void LoadUserTypeDropDown()
        {
            //Creating list of hard coded usertype from User type Enum
            var list = Enum.GetValues(typeof(UserTypeEnum)).Cast<UserTypeEnum>().ToList();
            cmbUserType.DataSource = list.ToList();
        }
        private void btnSaveUser_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUsername.Text))
            {
                return;
            }
            var model = new LoginModel();
            int id = 0;
            int.TryParse(txtUserId.Text.Trim().ToString(), out id);
            model.LoginId = id;
            model.FullName = txtFullName.Text.Trim().ToString();
            model.Email = txtEmail.Text.Trim().ToString();
            model.Phone = txtPhone.Text.Trim().ToString();
            //Get selected usertypeId from selected enum
            UserTypeEnum usertype;
            Enum.TryParse(cmbUserType.SelectedValue.ToString(), out usertype);
            model.UserTypeId = (int)usertype;
            model.Username = txtUsername.Text.Trim().ToString();
            model.Password = txtPassword.Text.Trim().ToString();
            model.IsLocker = checkBoxIsLocked.Checked;

            if (id == 0)
            {
                var createResult = LoginBl.Create(model);
                if (createResult)
                {
                    GetLoginList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                var editResult = LoginBl.Edit(model);
                if (editResult)
                {
                    GetLoginList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
        private void ResetForm()
        {
            txtUserId.Text = "";
            txtUsername.Text = "";
            txtPassword.Text = "";
            checkBoxIsLocked.Checked = false;
            cmbUserType.SelectedIndex = -1;
            btnDelete.Visible = false;
            txtFullName.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
        }

        private void dataGridViewUser_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            var model = new LoginModel();
            model.LoginId = int.Parse(dataGridViewUser.Rows[index].Cells[0].Value.ToString());
            model.UserTypeId = int.Parse(dataGridViewUser.Rows[index].Cells[1].Value.ToString());
            model.Username = dataGridViewUser.Rows[index].Cells[3].Value.ToString();
            model.IsLocker = dataGridViewUser.Rows[index].Cells[6].Value.ToString() == "Active" ? false : true;
            btnDelete.Visible = true;
            txtUserId.Text = model.LoginId.ToString();
            txtUsername.Text = model.Username;
            cmbUserType.SelectedItem = model.UserTypeTitle;
            checkBoxIsLocked.Checked = model.IsLocker;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int LoginId = int.Parse(txtUserId.Text.Trim());
            var deleteResult = LoginBl.Delete(LoginId);
            if (deleteResult)
            {
                GetLoginList();
                ResetForm();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }


        //private void cmbUserType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    UserTypeEnum usertype;
        //    Enum.TryParse<UserTypeEnum>(cmbUserType.SelectedValue.ToString(), out usertype);
        //    MessageBox.Show(((int)usertype).ToString());
        //}
    }
}
