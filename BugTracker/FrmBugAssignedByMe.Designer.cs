﻿namespace BugTracker
{
    partial class FrmBugAssignedByMe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBugAssignedByMe));
            this.dgvAssignedBugsList = new System.Windows.Forms.DataGridView();
            this.txtBugSearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssignedBugsList)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAssignedBugsList
            // 
            this.dgvAssignedBugsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAssignedBugsList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAssignedBugsList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgvAssignedBugsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAssignedBugsList.Location = new System.Drawing.Point(12, 41);
            this.dgvAssignedBugsList.MultiSelect = false;
            this.dgvAssignedBugsList.Name = "dgvAssignedBugsList";
            this.dgvAssignedBugsList.Size = new System.Drawing.Size(864, 404);
            this.dgvAssignedBugsList.TabIndex = 0;
            this.dgvAssignedBugsList.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvAssignedBugsList_RowHeaderMouseDoubleClick);
            // 
            // txtBugSearch
            // 
            this.txtBugSearch.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtBugSearch.Location = new System.Drawing.Point(71, 6);
            this.txtBugSearch.Name = "txtBugSearch";
            this.txtBugSearch.Size = new System.Drawing.Size(231, 29);
            this.txtBugSearch.TabIndex = 7;
            this.txtBugSearch.TextChanged += new System.EventHandler(this.txtBugSearch_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label2.Location = new System.Drawing.Point(8, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 21);
            this.label2.TabIndex = 6;
            this.label2.Text = "Search";
            // 
            // FrmBugAssignedByMe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 457);
            this.Controls.Add(this.txtBugSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvAssignedBugsList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmBugAssignedByMe";
            this.Text = "Bugs Reported";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmBugListAdmin_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssignedBugsList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAssignedBugsList;
        private System.Windows.Forms.TextBox txtBugSearch;
        private System.Windows.Forms.Label label2;
    }
}