﻿namespace BugTracker
{
    partial class FrmImageViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmImageViewer));
            this.picBoxImageViewer = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxImageViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // picBoxImageViewer
            // 
            this.picBoxImageViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picBoxImageViewer.Location = new System.Drawing.Point(0, 0);
            this.picBoxImageViewer.Name = "picBoxImageViewer";
            this.picBoxImageViewer.Size = new System.Drawing.Size(948, 535);
            this.picBoxImageViewer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBoxImageViewer.TabIndex = 0;
            this.picBoxImageViewer.TabStop = false;
            // 
            // FrmImageViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 535);
            this.Controls.Add(this.picBoxImageViewer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmImageViewer";
            this.Text = "Image Viewer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmImageViewer_FormClosed);
            this.Load += new System.EventHandler(this.FrmImageViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxImageViewer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxImageViewer;
    }
}