﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmSeverity : Form
    {
        SeverityBL severityBl = null;
        public FrmSeverity()
        {
            InitializeComponent();
            severityBl = new SeverityBL();
            GetSeverityList();
        }
        private static FrmSeverity severity = null;
        public static FrmSeverity Instance()
        {
            if (severity == null)
            {
                severity = new FrmSeverity();
            }
            return severity;
        }

        private void FrmSeverity_FormClosed(object sender, FormClosedEventArgs e)
        {
            severity = null;
        }

        private void GetSeverityList()
        {
            var severityList = severityBl.List();
            if (severityList.Any())
            {
                dataGridViewSeverity.DataSource = null;
                dataGridViewSeverity.DataSource = severityList.ToList();
            }
        }

        private void btnSaveSeverity_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSeverityTitle.Text))
            {
                return;
            }
            var model = new SeverityModel();
            int id = 0;
            int.TryParse(txtSeverityId.Text.Trim().ToString(), out id);
            model.SeverityId = id;
            model.Title = txtSeverityTitle.Text.Trim().ToString();
            model.Description = txtSeverityDescription.Text.Trim().ToString();
            if (id == 0)
            {
                var createResult = severityBl.Create(model);
                if (createResult)
                {
                    GetSeverityList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                var editResult = severityBl.Edit(model);
                if (editResult)
                {
                    GetSeverityList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
        private void ResetForm()
        {
            txtSeverityId.Text = "";
            txtSeverityDescription.Text = "";
            txtSeverityTitle.Text = "";
            btnDelete.Visible = false;
        }

        private void dataGridViewSeverity_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            var model = new SeverityModel();
            model.SeverityId = int.Parse(dataGridViewSeverity.Rows[index].Cells[0].Value.ToString());
            model.Title = dataGridViewSeverity.Rows[index].Cells[1].Value.ToString();
            model.Description = dataGridViewSeverity.Rows[index].Cells[2].Value.ToString();
            txtSeverityId.Text = model.SeverityId.ToString();
            txtSeverityTitle.Text = model.Title;
            txtSeverityDescription.Text = model.Description;
            btnDelete.Visible = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int severityId = int.Parse(txtSeverityId.Text.Trim());
            var deleteResult = severityBl.Delete(severityId);
            if (deleteResult)
            {
                GetSeverityList();
                ResetForm();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }
    }
}
