﻿namespace BugTracker
{
    partial class FrmPlatform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPlatform));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSavePlatform = new System.Windows.Forms.Button();
            this.txtPlatformDescription = new System.Windows.Forms.TextBox();
            this.txtPlatformTitle = new System.Windows.Forms.TextBox();
            this.txtPlatformId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewPlatform = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlatform)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnSavePlatform);
            this.panel1.Controls.Add(this.txtPlatformDescription);
            this.panel1.Controls.Add(this.txtPlatformTitle);
            this.panel1.Controls.Add(this.txtPlatformId);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 178);
            this.panel1.TabIndex = 0;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelete.Location = new System.Drawing.Point(333, 107);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 33);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSavePlatform
            // 
            this.btnSavePlatform.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnSavePlatform.Location = new System.Drawing.Point(252, 107);
            this.btnSavePlatform.Name = "btnSavePlatform";
            this.btnSavePlatform.Size = new System.Drawing.Size(75, 33);
            this.btnSavePlatform.TabIndex = 6;
            this.btnSavePlatform.Text = "Save";
            this.btnSavePlatform.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSavePlatform.UseVisualStyleBackColor = true;
            this.btnSavePlatform.Click += new System.EventHandler(this.btnSavePlatform_Click);
            // 
            // txtPlatformDescription
            // 
            this.txtPlatformDescription.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtPlatformDescription.Location = new System.Drawing.Point(252, 72);
            this.txtPlatformDescription.Name = "txtPlatformDescription";
            this.txtPlatformDescription.Size = new System.Drawing.Size(231, 29);
            this.txtPlatformDescription.TabIndex = 5;
            // 
            // txtPlatformTitle
            // 
            this.txtPlatformTitle.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtPlatformTitle.Location = new System.Drawing.Point(252, 37);
            this.txtPlatformTitle.Name = "txtPlatformTitle";
            this.txtPlatformTitle.Size = new System.Drawing.Size(231, 29);
            this.txtPlatformTitle.TabIndex = 4;
            // 
            // txtPlatformId
            // 
            this.txtPlatformId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtPlatformId.Location = new System.Drawing.Point(96, 2);
            this.txtPlatformId.Name = "txtPlatformId";
            this.txtPlatformId.Size = new System.Drawing.Size(231, 29);
            this.txtPlatformId.TabIndex = 3;
            this.txtPlatformId.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label3.Location = new System.Drawing.Point(97, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Platform Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label2.Location = new System.Drawing.Point(147, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Platform Title";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(11, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "PlatformId";
            this.label1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dataGridViewPlatform);
            this.panel2.Location = new System.Drawing.Point(12, 197);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(581, 224);
            this.panel2.TabIndex = 1;
            // 
            // dataGridViewPlatform
            // 
            this.dataGridViewPlatform.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewPlatform.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPlatform.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridViewPlatform.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPlatform.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewPlatform.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewPlatform.MultiSelect = false;
            this.dataGridViewPlatform.Name = "dataGridViewPlatform";
            this.dataGridViewPlatform.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewPlatform.Size = new System.Drawing.Size(575, 218);
            this.dataGridViewPlatform.TabIndex = 0;
            this.dataGridViewPlatform.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewPlatform_RowHeaderMouseClick);
            // 
            // FrmPlatform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 433);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPlatform";
            this.Text = "Platforms";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPlatform_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlatform)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtPlatformDescription;
        private System.Windows.Forms.TextBox txtPlatformTitle;
        private System.Windows.Forms.TextBox txtPlatformId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSavePlatform;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewPlatform;
        private System.Windows.Forms.Button btnDelete;
    }
}