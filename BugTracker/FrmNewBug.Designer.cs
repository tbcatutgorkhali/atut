﻿namespace BugTracker
{
    partial class FrmNewBug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmNewBug));
            this.label1 = new System.Windows.Forms.Label();
            this.txtBugId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSummary = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.cmbProductId = new System.Windows.Forms.ComboBox();
            this.cmbComponentId = new System.Windows.Forms.ComboBox();
            this.cmbVersionId = new System.Windows.Forms.ComboBox();
            this.cmbPlatformId = new System.Windows.Forms.ComboBox();
            this.btnBugSubmit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAttachmentText = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAttachImage = new System.Windows.Forms.Button();
            this.PicBoxAttachment = new System.Windows.Forms.PictureBox();
            this.cmbSeverity = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxAttachment)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "BugId";
            this.label1.Visible = false;
            // 
            // txtBugId
            // 
            this.txtBugId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtBugId.Location = new System.Drawing.Point(68, 12);
            this.txtBugId.Name = "txtBugId";
            this.txtBugId.Size = new System.Drawing.Size(238, 29);
            this.txtBugId.TabIndex = 1;
            this.txtBugId.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label3.Location = new System.Drawing.Point(113, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 21);
            this.label3.TabIndex = 3;
            this.label3.Text = "Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label4.Location = new System.Drawing.Point(140, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 21);
            this.label4.TabIndex = 4;
            this.label4.Text = "Version";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label7.Location = new System.Drawing.Point(113, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 21);
            this.label7.TabIndex = 7;
            this.label7.Text = "Component";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label8.Location = new System.Drawing.Point(474, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 21);
            this.label8.TabIndex = 8;
            this.label8.Text = "Platform";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label9.Location = new System.Drawing.Point(466, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 21);
            this.label9.TabIndex = 9;
            this.label9.Text = "Summary";
            // 
            // txtSummary
            // 
            this.txtSummary.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSummary.Location = new System.Drawing.Point(550, 86);
            this.txtSummary.Multiline = true;
            this.txtSummary.Name = "txtSummary";
            this.txtSummary.Size = new System.Drawing.Size(252, 109);
            this.txtSummary.TabIndex = 6;
            // 
            // txtDescription
            // 
            this.txtDescription.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtDescription.Location = new System.Drawing.Point(208, 121);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(336, 74);
            this.txtDescription.TabIndex = 7;
            // 
            // cmbProductId
            // 
            this.cmbProductId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProductId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbProductId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbProductId.Location = new System.Drawing.Point(208, 16);
            this.cmbProductId.Name = "cmbProductId";
            this.cmbProductId.Size = new System.Drawing.Size(252, 29);
            this.cmbProductId.TabIndex = 1;
            // 
            // cmbComponentId
            // 
            this.cmbComponentId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbComponentId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbComponentId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbComponentId.Location = new System.Drawing.Point(208, 86);
            this.cmbComponentId.Name = "cmbComponentId";
            this.cmbComponentId.Size = new System.Drawing.Size(252, 29);
            this.cmbComponentId.TabIndex = 5;
            // 
            // cmbVersionId
            // 
            this.cmbVersionId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVersionId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbVersionId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbVersionId.Location = new System.Drawing.Point(208, 51);
            this.cmbVersionId.Name = "cmbVersionId";
            this.cmbVersionId.Size = new System.Drawing.Size(252, 29);
            this.cmbVersionId.TabIndex = 3;
            // 
            // cmbPlatformId
            // 
            this.cmbPlatformId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPlatformId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPlatformId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbPlatformId.Location = new System.Drawing.Point(550, 16);
            this.cmbPlatformId.Name = "cmbPlatformId";
            this.cmbPlatformId.Size = new System.Drawing.Size(252, 29);
            this.cmbPlatformId.TabIndex = 2;
            // 
            // btnBugSubmit
            // 
            this.btnBugSubmit.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnBugSubmit.Location = new System.Drawing.Point(208, 474);
            this.btnBugSubmit.Name = "btnBugSubmit";
            this.btnBugSubmit.Size = new System.Drawing.Size(75, 29);
            this.btnBugSubmit.TabIndex = 10;
            this.btnBugSubmit.Text = "Submit";
            this.btnBugSubmit.UseVisualStyleBackColor = true;
            this.btnBugSubmit.Click += new System.EventHandler(this.btnBugSubmit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAttachmentText);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnAttachImage);
            this.groupBox1.Location = new System.Drawing.Point(208, 201);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(594, 267);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Attachment";
            // 
            // txtAttachmentText
            // 
            this.txtAttachmentText.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtAttachmentText.Location = new System.Drawing.Point(101, 54);
            this.txtAttachmentText.Multiline = true;
            this.txtAttachmentText.Name = "txtAttachmentText";
            this.txtAttachmentText.Size = new System.Drawing.Size(493, 207);
            this.txtAttachmentText.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label10.Location = new System.Drawing.Point(1, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 21);
            this.label10.TabIndex = 27;
            this.label10.Text = "Attach Code";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label6.Location = new System.Drawing.Point(43, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 21);
            this.label6.TabIndex = 27;
            this.label6.Text = "Image";
            // 
            // btnAttachImage
            // 
            this.btnAttachImage.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAttachImage.Location = new System.Drawing.Point(101, 19);
            this.btnAttachImage.Name = "btnAttachImage";
            this.btnAttachImage.Size = new System.Drawing.Size(169, 29);
            this.btnAttachImage.TabIndex = 8;
            this.btnAttachImage.Text = "Browse Attachment";
            this.btnAttachImage.UseVisualStyleBackColor = true;
            this.btnAttachImage.Click += new System.EventHandler(this.btnAttachImage_Click);
            // 
            // PicBoxAttachment
            // 
            this.PicBoxAttachment.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.PicBoxAttachment.Location = new System.Drawing.Point(808, 16);
            this.PicBoxAttachment.Name = "PicBoxAttachment";
            this.PicBoxAttachment.Size = new System.Drawing.Size(173, 99);
            this.PicBoxAttachment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicBoxAttachment.TabIndex = 27;
            this.PicBoxAttachment.TabStop = false;
            // 
            // cmbSeverity
            // 
            this.cmbSeverity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeverity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSeverity.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbSeverity.Location = new System.Drawing.Point(550, 51);
            this.cmbSeverity.Name = "cmbSeverity";
            this.cmbSeverity.Size = new System.Drawing.Size(252, 29);
            this.cmbSeverity.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label2.Location = new System.Drawing.Point(474, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 21);
            this.label2.TabIndex = 29;
            this.label2.Text = "Severity";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label5.Location = new System.Drawing.Point(140, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 21);
            this.label5.TabIndex = 30;
            this.label5.Text = "Project";
            // 
            // FrmNewBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 515);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbSeverity);
            this.Controls.Add(this.PicBoxAttachment);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnBugSubmit);
            this.Controls.Add(this.cmbPlatformId);
            this.Controls.Add(this.cmbVersionId);
            this.Controls.Add(this.cmbComponentId);
            this.Controls.Add(this.cmbProductId);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtSummary);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBugId);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmNewBug";
            this.Text = "File New Bug";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmNewBug_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxAttachment)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBugId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSummary;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.ComboBox cmbProductId;
        private System.Windows.Forms.ComboBox cmbComponentId;
        private System.Windows.Forms.ComboBox cmbVersionId;
        private System.Windows.Forms.ComboBox cmbPlatformId;
        private System.Windows.Forms.Button btnBugSubmit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtAttachmentText;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAttachImage;
        private System.Windows.Forms.PictureBox PicBoxAttachment;
        private System.Windows.Forms.ComboBox cmbSeverity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
    }
}