﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTrackerBL;
using System.IO;

namespace BugTracker
{
    public partial class FrmImageViewer : Form
    {
        AttachmentBL attachmentBl = null;
        public FrmImageViewer()
        {
            InitializeComponent();
            attachmentBl = new AttachmentBL();
        }
        static FrmImageViewer frmImageViewer = null;
        public static int AttachmentId;
        public static FrmImageViewer Instance()
        {
            if (frmImageViewer == null)
            {
                frmImageViewer = new FrmImageViewer();
            }
            return frmImageViewer;
        }
        private void FrmImageViewer_Load(object sender, EventArgs e)
        {
            byte[] imageData = attachmentBl.GetImage(AttachmentId);
            picBoxImageViewer.Image = byteToImage(imageData);
        }

        private void FrmImageViewer_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmImageViewer = null;
        }
        public static Image byteToImage(byte[] byt)
        {
            MemoryStream MSImage = new MemoryStream(byt);
            Image ReturnImage = Image.FromStream(MSImage);
            return ReturnImage;
        }
    }
}
