﻿using System;
using System.Text;
using System.Windows.Forms;
using BugTrackerBL;
using BugTrackerBL.Models;
using System.Threading;

namespace BugTracker
{
    public partial class FrmLogin : Form
    {
        LoginBL loginBl = null;
        public FrmLogin()
        {
            InitializeComponent();
            loginBl = new LoginBL();
        }

        private void CheckLogin(string user, string password, out string username)
        {
            username = "0";
            var pass = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(password));


        }
        //Login Module
        private void btnLogin_Click(object sender, EventArgs e)
        {
            LoginModel loginModel = new LoginModel();
            loginModel.Username = txtUsername.Text.Trim();
            loginModel.Password = txtPassword.Text.Trim();
            if (string.IsNullOrEmpty(loginModel.Username))
            {
                toolStripStatusLabel.Text = "Username field cannot be empty";
                return;
            }
            else if (string.IsNullOrEmpty(loginModel.Password))
            {
                toolStripStatusLabel.Text = "Password field cannot be empty";
                return;
            }
            string UserType;
            bool checkCredentials = loginBl.CheckLogin(loginModel, out UserType);
            if (checkCredentials)
            {
                if (UserType.ToLower() == UserTypeEnum.Admin.ToString().ToLower())
                {
                    BugTrackerMDI _bugTrackerMDI = new BugTrackerMDI();
                    _bugTrackerMDI.Show();
                    this.Hide();
                }
                //if (UserType.ToLower() == UserTypeEnum.Debugger.ToString().ToLower())
                else
                {
                    BugTrackerMDIUser _bugTrackerMDI = new BugTrackerMDIUser();
                    _bugTrackerMDI.Show();
                    this.Hide();
                }
            }
            else
            {
                toolStripStatusLabel.Text = "Invalid Username or Password";
                return;
            }
            return;
        }
    }
}
