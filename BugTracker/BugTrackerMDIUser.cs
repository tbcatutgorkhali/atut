﻿using BugTrackerBL.Models;
using System;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class BugTrackerMDIUser : Form
    {
        private string LoggedUser;
        private string LoggedUserType;
        public BugTrackerMDIUser()
        {
            InitializeComponent();
            this.LoggedUser = ActiveUser.Username ?? "Test User";
            toolStripStatusUser.Text = LoggedUser;
            LoggedUserType = ActiveUser.UserType ?? "Admin";
            ActiveUser.LoginId = ActiveUser.LoginId != 0 ? ActiveUser.LoginId : 1;
            if (ActiveUser.UserType.ToLower() == UserTypeEnum.QA.ToString().ToLower() || ActiveUser.UserType.ToLower() == UserTypeEnum.Specialist.ToString().ToLower() || ActiveUser.UserType.ToLower() == UserTypeEnum.Debugger.ToString().ToLower())
            {
                newBugToolStripMenuItem.Enabled = false;
                newBugToolStripMenuItem.Visible = false;
                assignedByMeToolStripMenuItem.Enabled = false;
                assignedByMeToolStripMenuItem.Visible = false;
            }
        }
        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void newBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmNewBug _newBug = FrmNewBug.Instance();
            _newBug.MdiParent = this;
            _newBug.Show();
        }
        private void BugTrackerDeBuggerMDI_Load(object sender, EventArgs e)
        {
            //FrmBugListUnAssigned _bugUnassigned = FrmBugListUnAssigned.Instance();
            //_bugUnassigned.MdiParent = this;
            //_bugUnassigned.Show();

            FrmBugAssignedToMe _bugassigned = FrmBugAssignedToMe.Instance();
            _bugassigned.MdiParent = this;
            _bugassigned.Show();
            if (ActiveUser.UserType.ToLower() == UserTypeEnum.Admin.ToString().ToLower() || ActiveUser.UserType.ToLower() == UserTypeEnum.Tester.ToString().ToLower())
            {
                FrmBugAssignedByMe _bugassignedBy = FrmBugAssignedByMe.Instance();
                _bugassignedBy.MdiParent = this;
                _bugassignedBy.Show();
            }
        }

        private void minimizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void assignedByMeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmBugAssignedByMe _bugassigned = FrmBugAssignedByMe.Instance();
            _bugassigned.MdiParent = this;
            _bugassigned.Show();
        }

        private void assignedBugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmBugAssignedToMe _bugassigned = FrmBugAssignedToMe.Instance();
            _bugassigned.MdiParent = this;
            _bugassigned.Show();
        }

        private void logOffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActiveUser.Username = "";
            ActiveUser.UserType = "";
            ActiveUser.UserTypeId = 0;
            ActiveUser.LoginId = 0;
            FrmLogin Login = new FrmLogin();
            Login.Show();
            this.Hide();
        }
    }
}
