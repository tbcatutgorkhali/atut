﻿namespace BugTracker
{
    partial class FrmComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmComponent));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSaveComponent = new System.Windows.Forms.Button();
            this.txtComponentDescription = new System.Windows.Forms.TextBox();
            this.txtComponentTitle = new System.Windows.Forms.TextBox();
            this.txtComponentId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewComponent = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComponent)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnSaveComponent);
            this.panel1.Controls.Add(this.txtComponentDescription);
            this.panel1.Controls.Add(this.txtComponentTitle);
            this.panel1.Controls.Add(this.txtComponentId);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 178);
            this.panel1.TabIndex = 0;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelete.Location = new System.Drawing.Point(346, 107);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 33);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSaveComponent
            // 
            this.btnSaveComponent.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnSaveComponent.Location = new System.Drawing.Point(265, 107);
            this.btnSaveComponent.Name = "btnSaveComponent";
            this.btnSaveComponent.Size = new System.Drawing.Size(75, 33);
            this.btnSaveComponent.TabIndex = 6;
            this.btnSaveComponent.Text = "Save";
            this.btnSaveComponent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSaveComponent.UseVisualStyleBackColor = true;
            this.btnSaveComponent.Click += new System.EventHandler(this.btnSaveComponent_Click);
            // 
            // txtComponentDescription
            // 
            this.txtComponentDescription.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtComponentDescription.Location = new System.Drawing.Point(265, 72);
            this.txtComponentDescription.Name = "txtComponentDescription";
            this.txtComponentDescription.Size = new System.Drawing.Size(231, 29);
            this.txtComponentDescription.TabIndex = 5;
            // 
            // txtComponentTitle
            // 
            this.txtComponentTitle.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtComponentTitle.Location = new System.Drawing.Point(265, 37);
            this.txtComponentTitle.Name = "txtComponentTitle";
            this.txtComponentTitle.Size = new System.Drawing.Size(231, 29);
            this.txtComponentTitle.TabIndex = 4;
            // 
            // txtComponentId
            // 
            this.txtComponentId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtComponentId.Location = new System.Drawing.Point(122, 2);
            this.txtComponentId.Name = "txtComponentId";
            this.txtComponentId.Size = new System.Drawing.Size(231, 29);
            this.txtComponentId.TabIndex = 3;
            this.txtComponentId.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label3.Location = new System.Drawing.Point(84, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Component Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label2.Location = new System.Drawing.Point(134, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Component Title";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(11, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "ComponentId";
            this.label1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dataGridViewComponent);
            this.panel2.Location = new System.Drawing.Point(12, 197);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(581, 224);
            this.panel2.TabIndex = 1;
            // 
            // dataGridViewComponent
            // 
            this.dataGridViewComponent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewComponent.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewComponent.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridViewComponent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewComponent.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewComponent.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewComponent.MultiSelect = false;
            this.dataGridViewComponent.Name = "dataGridViewComponent";
            this.dataGridViewComponent.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewComponent.Size = new System.Drawing.Size(575, 218);
            this.dataGridViewComponent.TabIndex = 0;
            this.dataGridViewComponent.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewComponent_RowHeaderMouseClick);
            // 
            // FrmComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 433);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmComponent";
            this.Text = "Components";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmComponent_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComponent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtComponentDescription;
        private System.Windows.Forms.TextBox txtComponentTitle;
        private System.Windows.Forms.TextBox txtComponentId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveComponent;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewComponent;
        private System.Windows.Forms.Button btnDelete;
    }
}