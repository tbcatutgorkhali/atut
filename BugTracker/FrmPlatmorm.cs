﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmPlatform : Form
    {
        PlatformBL PlatformBl = null;
        public FrmPlatform()
        {
            InitializeComponent();
            PlatformBl = new PlatformBL();
            GetPlatformList();
        }
        private static FrmPlatform Platform = null;
        public static FrmPlatform Instance()
        {
            if (Platform == null)
            {
                Platform = new FrmPlatform();
            }
            return Platform;
        }

        private void FrmPlatform_FormClosed(object sender, FormClosedEventArgs e)
        {
            Platform = null;
        }

        private void GetPlatformList()
        {
            var PlatformList = PlatformBl.List();
            if (PlatformList.Any())
            {
                dataGridViewPlatform.DataSource = null;
                dataGridViewPlatform.DataSource = PlatformList.ToList();
            }
        }

        private void btnSavePlatform_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPlatformTitle.Text))
            {
                return;
            }
            var model = new PlatformModel();
            int id = 0;
            int.TryParse(txtPlatformId.Text.Trim().ToString(), out id);
            model.PlatformId = id;
            model.Title = txtPlatformTitle.Text.Trim().ToString();
            model.Description = txtPlatformDescription.Text.Trim().ToString();
            if (id == 0)
            {
                var createResult = PlatformBl.Create(model);
                if (createResult)
                {
                    GetPlatformList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                var editResult = PlatformBl.Edit(model);
                if (editResult)
                {
                    GetPlatformList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
        private void ResetForm()
        {
            txtPlatformId.Text = "";
            txtPlatformDescription.Text = "";
            txtPlatformTitle.Text = "";
            btnDelete.Visible = false;
        }

        private void dataGridViewPlatform_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            var model = new PlatformModel();
            model.PlatformId = int.Parse(dataGridViewPlatform.Rows[index].Cells[0].Value.ToString());
            model.Title = dataGridViewPlatform.Rows[index].Cells[1].Value.ToString();
            model.Description = dataGridViewPlatform.Rows[index].Cells[2].Value.ToString();
            txtPlatformId.Text = model.PlatformId.ToString();
            txtPlatformTitle.Text = model.Title;
            txtPlatformDescription.Text = model.Description;
            btnDelete.Visible = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int PlatformId = int.Parse(txtPlatformId.Text.Trim());
            var deleteResult = PlatformBl.Delete(PlatformId);
            if (deleteResult)
            {
                GetPlatformList();
                ResetForm();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }
    }
}
