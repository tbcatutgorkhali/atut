﻿namespace BugTracker
{
    partial class FrmSeverity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSeverityId = new System.Windows.Forms.TextBox();
            this.txtSeverityTitle = new System.Windows.Forms.TextBox();
            this.txtSeverityDescription = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSaveSeverity = new System.Windows.Forms.Button();
            this.dataGridViewSeverity = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSeverity)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnSaveSeverity);
            this.panel1.Controls.Add(this.txtSeverityDescription);
            this.panel1.Controls.Add(this.txtSeverityTitle);
            this.panel1.Controls.Add(this.txtSeverityId);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 178);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(11, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "SeverityId";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label2.Location = new System.Drawing.Point(147, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Severity Title";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label3.Location = new System.Drawing.Point(97, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Severity Description";
            // 
            // txtSeverityId
            // 
            this.txtSeverityId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSeverityId.Location = new System.Drawing.Point(96, 2);
            this.txtSeverityId.Name = "txtSeverityId";
            this.txtSeverityId.Size = new System.Drawing.Size(231, 29);
            this.txtSeverityId.TabIndex = 3;
            this.txtSeverityId.Visible = false;
            // 
            // txtSeverityTitle
            // 
            this.txtSeverityTitle.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSeverityTitle.Location = new System.Drawing.Point(252, 37);
            this.txtSeverityTitle.Name = "txtSeverityTitle";
            this.txtSeverityTitle.Size = new System.Drawing.Size(231, 29);
            this.txtSeverityTitle.TabIndex = 4;
            // 
            // txtSeverityDescription
            // 
            this.txtSeverityDescription.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSeverityDescription.Location = new System.Drawing.Point(252, 72);
            this.txtSeverityDescription.Name = "txtSeverityDescription";
            this.txtSeverityDescription.Size = new System.Drawing.Size(231, 29);
            this.txtSeverityDescription.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dataGridViewSeverity);
            this.panel2.Location = new System.Drawing.Point(12, 197);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(581, 224);
            this.panel2.TabIndex = 1;
            // 
            // btnSaveSeverity
            // 
            this.btnSaveSeverity.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnSaveSeverity.Location = new System.Drawing.Point(252, 107);
            this.btnSaveSeverity.Name = "btnSaveSeverity";
            this.btnSaveSeverity.Size = new System.Drawing.Size(75, 33);
            this.btnSaveSeverity.TabIndex = 6;
            this.btnSaveSeverity.Text = "Save";
            this.btnSaveSeverity.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSaveSeverity.UseVisualStyleBackColor = true;
            this.btnSaveSeverity.Click += new System.EventHandler(this.btnSaveSeverity_Click);
            // 
            // dataGridViewSeverity
            // 
            this.dataGridViewSeverity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSeverity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSeverity.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridViewSeverity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSeverity.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewSeverity.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewSeverity.MultiSelect = false;
            this.dataGridViewSeverity.Name = "dataGridViewSeverity";
            this.dataGridViewSeverity.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewSeverity.Size = new System.Drawing.Size(575, 218);
            this.dataGridViewSeverity.TabIndex = 0;
            this.dataGridViewSeverity.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewSeverity_RowHeaderMouseClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelete.Location = new System.Drawing.Point(333, 107);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 33);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // FrmSeverity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 433);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSeverity";
            this.Text = "Bug Severity Types";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmSeverity_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSeverity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtSeverityDescription;
        private System.Windows.Forms.TextBox txtSeverityTitle;
        private System.Windows.Forms.TextBox txtSeverityId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveSeverity;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewSeverity;
        private System.Windows.Forms.Button btnDelete;
    }
}