﻿using BugTrackerBL;
using BugTrackerBL.Models;
using FastColoredTextBoxNS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmBugDetails : Form
    {
        TextStyle BlueStyle = new TextStyle(Brushes.Blue, null, FontStyle.Regular);
        TextStyle BoldStyle = new TextStyle(null, null, FontStyle.Bold | FontStyle.Underline);
        TextStyle GrayStyle = new TextStyle(Brushes.Gray, null, FontStyle.Regular);
        TextStyle MagentaStyle = new TextStyle(Brushes.Magenta, null, FontStyle.Regular);
        TextStyle GreenStyle = new TextStyle(Brushes.Green, null, FontStyle.Italic);
        TextStyle BrownStyle = new TextStyle(Brushes.Brown, null, FontStyle.Italic);
        TextStyle MaroonStyle = new TextStyle(Brushes.Maroon, null, FontStyle.Regular);
        MarkerStyle SameWordsStyle = new MarkerStyle(new SolidBrush(Color.FromArgb(40, Color.Gray)));

        private void InitStylesPriority()
        {
            //add this style explicitly for drawing under other styles
            fctb.AddStyle(SameWordsStyle);
        }
        private ProjectBL projectBl = null;
        private PlatformBL platformBl = null;
        private VersionBL versionBl = null;
        private ComponentBL componentBl = null;
        private AttachmentBL attachmentBl = null;
        private SeverityBL severityBl = null;
        private StatusBL statusBl = null;
        private BugBL bugBL = null;
        private BugCommentsBL bugCommentBl = null;
        public FrmBugDetails()
        {
            InitializeComponent();
            projectBl = new ProjectBL();
            platformBl = new PlatformBL();
            versionBl = new VersionBL();
            componentBl = new ComponentBL();
            attachmentBl = new AttachmentBL();
            severityBl = new SeverityBL();
            statusBl = new StatusBL();
            bugBL = new BugBL();
            bugCommentBl = new BugCommentsBL();
            GetProjecList();
            GetPlatformList();
            GetVersionList();
            GetComponentList();
            GetStatusList();
            GetSeverityList();
            ClearUI();
        }

        private void GetCommentsList()
        {
            var bugCommentsList = bugCommentBl.List(dataModel.BugId);
            dgvComments.DataSource = null;
            dgvComments.DataSource = bugCommentsList;
        }

        public static BugModel dataModel;
        private static FrmBugDetails BugDetails = null;
        public static FrmBugDetails Instance()
        {
            if (BugDetails == null)
            {
                BugDetails = new FrmBugDetails();
            }
            return BugDetails;
        }

        private void FrmBugDetails_FormClosed(object sender, FormClosedEventArgs e)
        {
            BugDetails = null;
            dataModel = null;
        }

        private void btnAttachImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog ImageDialog = new OpenFileDialog();
            ImageDialog.Filter = "Image Files|*.jpg;*.png;*.gif";
            if (ImageDialog.ShowDialog() == DialogResult.OK && ImageDialog.FileName.Length > 0)
            {
                PicBoxAttachment.Image = Image.FromFile(ImageDialog.FileName);
            }
        }
        private void GetProjecList()
        {
            var projectList = projectBl.List();
            if (projectList.Any())
            {
                cmbProductId.DataSource = projectList.ToList();
                cmbProductId.DisplayMember = "Title";
                cmbProductId.ValueMember = "ProjectId";
            }
        }
        private void GetStatusList()
        {
            var projectList = statusBl.List();
            if (projectList.Any())
            {
                cmbStatus.DataSource = projectList.ToList();
                cmbStatus.DisplayMember = "Title";
                cmbStatus.ValueMember = "StatusId";
            }
        }
        private void GetSeverityList()
        {
            var projectList = projectBl.List();
            if (projectList.Any())
            {
                cmbSeverity.DataSource = severityBl.List();
                cmbSeverity.DisplayMember = "Title";
                cmbSeverity.ValueMember = "SeverityId";
            }
        }

        private void GetComponentList()
        {
            var projectList = projectBl.List();
            if (projectList.Any())
            {
                cmbComponentId.DataSource = componentBl.List();
                cmbComponentId.DisplayMember = "Title";
                cmbComponentId.ValueMember = "ComponentId";
            }
        }
        private void GetPlatformList()
        {
            var projectList = projectBl.List();
            if (projectList.Any())
            {
                cmbPlatformId.DataSource = platformBl.List();
                cmbPlatformId.DisplayMember = "Title";
                cmbPlatformId.ValueMember = "PlatformId";
            }
        }
        private void GetVersionList()
        {
            var projectList = versionBl.List();
            if (projectList.Any())
            {
                cmbVersionId.DataSource = versionBl.List();
                cmbVersionId.DisplayMember = "Title";
                cmbVersionId.ValueMember = "VersionId";
            }
        }
        public static Image byteToImage(byte[] byt)
        {
            MemoryStream MSImage = new MemoryStream(byt);
            Image ReturnImage = Image.FromStream(MSImage);
            return ReturnImage;
        }
        public static byte[] ImageToByte(Image image)
        {
            MemoryStream MS = new MemoryStream();
            image.Save(MS, System.Drawing.Imaging.ImageFormat.Jpeg);
            return MS.ToArray();

        }

        private void btnBugSubmit_Click(object sender, EventArgs e)
        {
            var bugModel = new BugModel();
            var attachmentModel = new AttachmentModel();

            if (PicBoxAttachment.Image != null || txtAttachmentText.Text != "")
            {
                attachmentModel.AttachmentFileName = ImageToByte(PicBoxAttachment.Image);
                attachmentModel.AttachementText = txtAttachmentText.Text.Trim();
            }
            int attachmentId = 0;
            attachmentBl.Create(attachmentModel, out attachmentId);
            bugModel.AttachmentId = attachmentId;
            bugModel.ProjectId = int.Parse(cmbProductId.SelectedValue.ToString());
            bugModel.ComponentId = int.Parse(cmbComponentId.SelectedValue.ToString());
            bugModel.VersionId = int.Parse(cmbVersionId.SelectedValue.ToString());
            bugModel.SeverityId = int.Parse(cmbSeverity.SelectedValue.ToString());
            bugModel.PlatformId = int.Parse(cmbPlatformId.SelectedValue.ToString());
            bugModel.Sumary = txtSummary.Text.Trim();
            bugModel.Description = txtDescription.Text.Trim();
            bugModel.ReporterId = ActiveUser.LoginId;
            int bugId;
            var bugCreateResult = bugBL.Create(bugModel, out bugId);
            if (bugCreateResult)
            {
                MessageBox.Show("Bug Reported Successfully, Admin will approve this bug after review.", "Success");
            }
            ClearUI();

        }
        private void ClearUI()
        {
            txtBugId.Text = "";
            txtDescription.Text = "";
            txtSummary.Text = "";
            txtAttachmentText.Text = "";
            cmbComponentId.SelectedIndex = -1;
            cmbPlatformId.SelectedIndex = -1;
            cmbProductId.SelectedIndex = -1;
            cmbSeverity.SelectedIndex = -1;
            cmbVersionId.SelectedIndex = -1;
            cmbStatus.SelectedIndex = -1;
            PicBoxAttachment.Image = null;
            toolStripStatusLabel1.Text = "";
        }

        private void FrmBugDetailsAdmin_Load(object sender, EventArgs e)
        {
            txtBugId.Text = dataModel.BugId.ToString();
            txtDescription.Text = dataModel.Description.ToString() != null ? dataModel.Description.ToString() : "";
            txtSummary.Text = dataModel.Sumary.ToString() ?? "";
            txtAttachmentText.Text = dataModel.AttachementText != null ? dataModel.AttachementText : "";
            cmbComponentId.SelectedValue = dataModel.ComponentId;
            cmbPlatformId.SelectedValue = dataModel.PlatformId;
            cmbProductId.SelectedValue = dataModel.ProjectId;
            cmbSeverity.SelectedValue = dataModel.SeverityId;
            cmbVersionId.SelectedValue = dataModel.VersionId;
            cmbStatus.SelectedValue = dataModel.StatusId;
            PicBoxAttachment.Image = dataModel.AttachmentFileName != null ? byteToImage(dataModel.AttachmentFileName) : null;
            fctb.Text = dataModel.AttachementText != null ? dataModel.AttachementText : "";
            GetCommentsList();
        }
        private void PicBoxAttachment_Click(object sender, EventArgs e)
        {
            FrmImageViewer _imageViewer = FrmImageViewer.Instance();
            FrmImageViewer.AttachmentId = dataModel.AttachmentId ?? 0;
            _imageViewer.MdiParent = this.ParentForm;
            _imageViewer.Show();

        }

        private void btnBugUpdate_Click(object sender, EventArgs e)
        {
            var attachmentModel = new AttachmentModel();
            if (PicBoxAttachment.Image != null || txtAttachmentText.Text != "")
            {

                attachmentModel.AttachmentFileName = PicBoxAttachment.Image != null ? ImageToByte(PicBoxAttachment.Image) : null;
                attachmentModel.AttachementText = txtAttachmentText.Text.Trim();
                attachmentModel.AttachmentId = dataModel.AttachmentId ?? 0;
            }
            int attachmentId = 0;
            if (attachmentModel.AttachmentId > 0)
            {
                attachmentBl.Edit(attachmentModel);
            }
            else
            {
                attachmentBl.Create(attachmentModel, out attachmentId);
            }
            var bugModel = new BugModel();
            bugModel.BugId = dataModel.BugId;
            bugModel.ProjectId = int.Parse(cmbProductId.SelectedValue.ToString());
            bugModel.ComponentId = int.Parse(cmbComponentId.SelectedValue.ToString());
            bugModel.VersionId = int.Parse(cmbVersionId.SelectedValue.ToString());
            bugModel.SeverityId = int.Parse(cmbSeverity.SelectedValue.ToString());
            bugModel.PlatformId = int.Parse(cmbPlatformId.SelectedValue.ToString());
            bugModel.Sumary = txtSummary.Text.Trim();
            bugModel.Description = txtDescription.Text.Trim();
            bugModel.ReporterId = ActiveUser.LoginId;
            bugModel.AttachmentId = attachmentId > 0 ? attachmentId : dataModel.AttachmentId > 0 ? dataModel.AttachmentId : null;
            var bugCreateResult = bugBL.Edit(bugModel);
            if (bugCreateResult)
            {
                MessageBox.Show("Bug updated Successfully, Admin will approve this bug after review.", "Success");
            }
            else
            {
                MessageBox.Show("Bug update failed!", "Failed");
            }
        }

        private void txtAttachmentText_TextChanged(object sender, EventArgs e)
        {
            fctb.Text = txtAttachmentText.Text;
        }

        private void fctb_TextChanged(object sender, FastColoredTextBoxNS.TextChangedEventArgs e)
        {
            txtAttachmentText.Text = fctb.Text;
            var lang = "CSharp (custom highlighter)";
            switch (lang)
            {
                case "CSharp (custom highlighter)":
                    //For sample, we will highlight the syntax of C# manually, although could use built-in highlighter
                    CSharpSyntaxHighlight(e);//custom highlighting
                    break;
                default:
                    break;//for highlighting of other languages, we using built-in FastColoredTextBox highlighter
            }

            if (fctb.Text.Trim().StartsWith("<?xml"))
            {
                fctb.Language = Language.XML;

                fctb.ClearStylesBuffer();
                fctb.Range.ClearStyle(StyleIndex.All);
                InitStylesPriority();
                fctb.AutoIndentNeeded -= fctb_AutoIndentNeeded;

                fctb.OnSyntaxHighlight(new TextChangedEventArgs(fctb.Range));
            }
        }
        private void CSharpSyntaxHighlight(TextChangedEventArgs e)
        {
            fctb.LeftBracket = '(';
            fctb.RightBracket = ')';
            fctb.LeftBracket2 = '\x0';
            fctb.RightBracket2 = '\x0';
            //clear style of changed range
            e.ChangedRange.ClearStyle(BlueStyle, BoldStyle, GrayStyle, MagentaStyle, GreenStyle, BrownStyle);

            //string highlighting
            e.ChangedRange.SetStyle(BrownStyle, @"""""|@""""|''|@"".*?""|(?<!@)(?<range>"".*?[^\\]"")|'.*?[^\\]'");
            //comment highlighting
            e.ChangedRange.SetStyle(GreenStyle, @"//.*$", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(GreenStyle, @"(/\*.*?\*/)|(/\*.*)", RegexOptions.Singleline);
            e.ChangedRange.SetStyle(GreenStyle, @"(/\*.*?\*/)|(.*\*/)", RegexOptions.Singleline | RegexOptions.RightToLeft);
            //number highlighting
            e.ChangedRange.SetStyle(MagentaStyle, @"\b\d+[\.]?\d*([eE]\-?\d+)?[lLdDfF]?\b|\b0x[a-fA-F\d]+\b");
            //attribute highlighting
            e.ChangedRange.SetStyle(GrayStyle, @"^\s*(?<range>\[.+?\])\s*$", RegexOptions.Multiline);
            //class name highlighting
            e.ChangedRange.SetStyle(BoldStyle, @"\b(class|struct|enum|interface)\s+(?<range>\w+?)\b");
            //keyword highlighting
            e.ChangedRange.SetStyle(BlueStyle, @"\b(abstract|as|base|bool|break|byte|case|catch|char|checked|class|const|continue|decimal|default|delegate|do|double|else|enum|event|explicit|extern|false|finally|fixed|float|for|foreach|goto|if|implicit|in|int|interface|internal|is|lock|long|namespace|new|null|object|operator|out|override|params|private|protected|public|readonly|ref|return|sbyte|sealed|short|sizeof|stackalloc|static|string|struct|switch|this|throw|true|try|typeof|uint|ulong|unchecked|unsafe|ushort|using|virtual|void|volatile|while|add|alias|ascending|descending|dynamic|from|get|global|group|into|join|let|orderby|partial|remove|select|set|value|var|where|yield)\b|#region\b|#endregion\b");

            //clear folding markers
            e.ChangedRange.ClearFoldingMarkers();

            //set folding markers
            e.ChangedRange.SetFoldingMarkers("{", "}");//allow to collapse brackets block
            e.ChangedRange.SetFoldingMarkers(@"#region\b", @"#endregion\b");//allow to collapse #region blocks
            e.ChangedRange.SetFoldingMarkers(@"/\*", @"\*/");//allow to collapse comment block
        }
        private void fctb_AutoIndentNeeded(object sender, AutoIndentEventArgs args)
        {
            //block {}
            if (Regex.IsMatch(args.LineText, @"^[^""']*\{.*\}[^""']*$"))
                return;
            //start of block {}
            if (Regex.IsMatch(args.LineText, @"^[^""']*\{"))
            {
                args.ShiftNextLines = args.TabLength;
                return;
            }
            //end of block {}
            if (Regex.IsMatch(args.LineText, @"}[^""']*$"))
            {
                args.Shift = -args.TabLength;
                args.ShiftNextLines = -args.TabLength;
                return;
            }
            //label
            if (Regex.IsMatch(args.LineText, @"^\s*\w+\s*:\s*($|//)") &&
                !Regex.IsMatch(args.LineText, @"^\s*default\s*:"))
            {
                args.Shift = -args.TabLength;
                return;
            }
            //some statements: case, default
            if (Regex.IsMatch(args.LineText, @"^\s*(case|default)\b.*:\s*($|//)"))
            {
                args.Shift = -args.TabLength / 2;
                return;
            }
            //is unclosed operator in previous line ?
            if (Regex.IsMatch(args.PrevLineText, @"^\s*(if|for|foreach|while|[\}\s]*else)\b[^{]*$"))
                if (!Regex.IsMatch(args.PrevLineText, @"(;\s*$)|(;\s*//)"))//operator is unclosed
                {
                    args.Shift = args.TabLength;
                    return;
                }
        }

        private void btnAddComment_Click(object sender, EventArgs e)
        {
            FrmBugComments _BugComments = FrmBugComments.Instance();
            FrmBugComments.BugId = dataModel.BugId;
            FrmBugComments.RepositoryIssueId = dataModel.VersionControlIssueId ?? 0;
            _BugComments.MdiParent = this.ParentForm;
            _BugComments.Show();
        }
        public static FrmBugDetails CloseFrmBugDetails()
        {
            return BugDetails;
        }

        private void btnAssignment_Click(object sender, EventArgs e)
        {
            FrmBugAssignment _BugAssignment = FrmBugAssignment.Instance();
            FrmBugAssignment.BugId = dataModel.BugId;
            _BugAssignment.MdiParent = this.ParentForm;
            _BugAssignment.Show();
        }

        private void btnSetStatus_Click(object sender, EventArgs e)
        {
            if (int.Parse(cmbStatus.SelectedValue.ToString()) > 1)
            {
                if (bugBL.statusChange(dataModel.BugId, int.Parse(cmbStatus.SelectedValue.ToString()))) {
                    toolStripStatusLabel1.Text = "Status Updated Successfully";
                } 
            }
            else
            {
                toolStripStatusLabel1.Text = "Select valid status for bug first.";
            }
        }
    }
}
