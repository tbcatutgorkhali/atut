﻿namespace BugTracker
{
    partial class FrmBugAssignment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBugAssignment));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbAssginTo = new System.Windows.Forms.ComboBox();
            this.cmbBugId = new System.Windows.Forms.ComboBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSaveBugAssignment = new System.Windows.Forms.Button();
            this.txtBugAssignmentId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewBugAssignment = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBugAssignment)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cmbAssginTo);
            this.panel1.Controls.Add(this.cmbBugId);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnSaveBugAssignment);
            this.panel1.Controls.Add(this.txtBugAssignmentId);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 178);
            this.panel1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label4.Location = new System.Drawing.Point(166, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 21);
            this.label4.TabIndex = 10;
            this.label4.Text = "Bug";
            // 
            // cmbAssginTo
            // 
            this.cmbAssginTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAssginTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAssginTo.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbAssginTo.Location = new System.Drawing.Point(215, 73);
            this.cmbAssginTo.Name = "cmbAssginTo";
            this.cmbAssginTo.Size = new System.Drawing.Size(231, 29);
            this.cmbAssginTo.TabIndex = 9;
            // 
            // cmbBugId
            // 
            this.cmbBugId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBugId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbBugId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbBugId.Location = new System.Drawing.Point(215, 38);
            this.cmbBugId.Name = "cmbBugId";
            this.cmbBugId.Size = new System.Drawing.Size(231, 29);
            this.cmbBugId.TabIndex = 8;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelete.Location = new System.Drawing.Point(296, 108);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 33);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSaveBugAssignment
            // 
            this.btnSaveBugAssignment.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnSaveBugAssignment.Location = new System.Drawing.Point(215, 108);
            this.btnSaveBugAssignment.Name = "btnSaveBugAssignment";
            this.btnSaveBugAssignment.Size = new System.Drawing.Size(75, 33);
            this.btnSaveBugAssignment.TabIndex = 6;
            this.btnSaveBugAssignment.Text = "Save";
            this.btnSaveBugAssignment.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSaveBugAssignment.UseVisualStyleBackColor = true;
            this.btnSaveBugAssignment.Click += new System.EventHandler(this.btnSaveBugAssignment_Click);
            // 
            // txtBugAssignmentId
            // 
            this.txtBugAssignmentId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtBugAssignmentId.Location = new System.Drawing.Point(149, 2);
            this.txtBugAssignmentId.Name = "txtBugAssignmentId";
            this.txtBugAssignmentId.Size = new System.Drawing.Size(231, 29);
            this.txtBugAssignmentId.TabIndex = 3;
            this.txtBugAssignmentId.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label3.Location = new System.Drawing.Point(134, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Assign To";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(11, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "BugAssignmentId";
            this.label1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dataGridViewBugAssignment);
            this.panel2.Location = new System.Drawing.Point(12, 197);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(581, 224);
            this.panel2.TabIndex = 1;
            // 
            // dataGridViewBugAssignment
            // 
            this.dataGridViewBugAssignment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewBugAssignment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewBugAssignment.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridViewBugAssignment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBugAssignment.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewBugAssignment.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewBugAssignment.MultiSelect = false;
            this.dataGridViewBugAssignment.Name = "dataGridViewBugAssignment";
            this.dataGridViewBugAssignment.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewBugAssignment.Size = new System.Drawing.Size(575, 218);
            this.dataGridViewBugAssignment.TabIndex = 0;
            this.dataGridViewBugAssignment.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewBugAssignment_RowHeaderMouseClick);
            // 
            // FrmBugAssignment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 433);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmBugAssignment";
            this.Text = "Project Accessignment";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmBugAssignment_FormClosed);
            this.Load += new System.EventHandler(this.FrmBugAssignment_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBugAssignment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtBugAssignmentId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveBugAssignment;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewBugAssignment;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cmbBugId;
        private System.Windows.Forms.ComboBox cmbAssginTo;
        private System.Windows.Forms.Label label4;
    }
}