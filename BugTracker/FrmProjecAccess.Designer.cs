﻿namespace BugTracker
{
    partial class FrmProjecAccess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProjecAccess));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbAssginTo = new System.Windows.Forms.ComboBox();
            this.cmbProjectTitle = new System.Windows.Forms.ComboBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSaveProjecAccess = new System.Windows.Forms.Button();
            this.txtProjecAccessId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewProjecAccess = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProjecAccess)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.cmbAssginTo);
            this.panel1.Controls.Add(this.cmbProjectTitle);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnSaveProjecAccess);
            this.panel1.Controls.Add(this.txtProjecAccessId);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 178);
            this.panel1.TabIndex = 0;
            // 
            // cmbAssginTo
            // 
            this.cmbAssginTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAssginTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAssginTo.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbAssginTo.Location = new System.Drawing.Point(252, 72);
            this.cmbAssginTo.Name = "cmbAssginTo";
            this.cmbAssginTo.Size = new System.Drawing.Size(231, 29);
            this.cmbAssginTo.TabIndex = 9;
            // 
            // cmbProjectTitle
            // 
            this.cmbProjectTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProjectTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbProjectTitle.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbProjectTitle.Location = new System.Drawing.Point(252, 37);
            this.cmbProjectTitle.Name = "cmbProjectTitle";
            this.cmbProjectTitle.Size = new System.Drawing.Size(231, 29);
            this.cmbProjectTitle.TabIndex = 8;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelete.Location = new System.Drawing.Point(333, 107);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 33);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSaveProjecAccess
            // 
            this.btnSaveProjecAccess.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnSaveProjecAccess.Location = new System.Drawing.Point(252, 107);
            this.btnSaveProjecAccess.Name = "btnSaveProjecAccess";
            this.btnSaveProjecAccess.Size = new System.Drawing.Size(75, 33);
            this.btnSaveProjecAccess.TabIndex = 6;
            this.btnSaveProjecAccess.Text = "Save";
            this.btnSaveProjecAccess.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSaveProjecAccess.UseVisualStyleBackColor = true;
            this.btnSaveProjecAccess.Click += new System.EventHandler(this.btnSaveProjecAccess_Click);
            // 
            // txtProjecAccessId
            // 
            this.txtProjecAccessId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtProjecAccessId.Location = new System.Drawing.Point(96, 2);
            this.txtProjecAccessId.Name = "txtProjecAccessId";
            this.txtProjecAccessId.Size = new System.Drawing.Size(231, 29);
            this.txtProjecAccessId.TabIndex = 3;
            this.txtProjecAccessId.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label3.Location = new System.Drawing.Point(171, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Assign To";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label2.Location = new System.Drawing.Point(155, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Project Title";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(11, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "ProjecAccessId";
            this.label1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dataGridViewProjecAccess);
            this.panel2.Location = new System.Drawing.Point(12, 197);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(581, 224);
            this.panel2.TabIndex = 1;
            // 
            // dataGridViewProjecAccess
            // 
            this.dataGridViewProjecAccess.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewProjecAccess.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewProjecAccess.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridViewProjecAccess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProjecAccess.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewProjecAccess.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewProjecAccess.MultiSelect = false;
            this.dataGridViewProjecAccess.Name = "dataGridViewProjecAccess";
            this.dataGridViewProjecAccess.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewProjecAccess.Size = new System.Drawing.Size(575, 218);
            this.dataGridViewProjecAccess.TabIndex = 0;
            this.dataGridViewProjecAccess.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewProjecAccess_RowHeaderMouseClick);
            // 
            // FrmProjecAccess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 433);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmProjecAccess";
            this.Text = "Project Accessignment";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmProjecAccess_FormClosed);
            this.Load += new System.EventHandler(this.FrmProjecAccess_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProjecAccess)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtProjecAccessId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveProjecAccess;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewProjecAccess;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cmbProjectTitle;
        private System.Windows.Forms.ComboBox cmbAssginTo;
    }
}