﻿namespace BugTracker
{
    partial class BugTrackerMDI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripMainUsername = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.myDashboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unAssignedBugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignedBugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newBugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.platformToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.projectAssignmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bugToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bugAssignmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.componentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.myDashboardToolStripMenuItem,
            this.newBugToolStripMenuItem,
            this.settingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1213, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMainUsername,
            this.toolStripStatusLabel2,
            this.toolStripStatusUser});
            this.statusStrip1.Location = new System.Drawing.Point(0, 421);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1213, 26);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripMainUsername
            // 
            this.toolStripMainUsername.Name = "toolStripMainUsername";
            this.toolStripMainUsername.Size = new System.Drawing.Size(0, 21);
            // 
            // toolStripStatusUser
            // 
            this.toolStripStatusUser.Name = "toolStripStatusUser";
            this.toolStripStatusUser.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripStatusUser.Size = new System.Drawing.Size(0, 21);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripStatusLabel2.Image = global::BugTracker.Properties.Resources._1478369675_060_Off;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(50, 21);
            this.toolStripStatusLabel2.Text = "Exit";
            this.toolStripStatusLabel2.Click += new System.EventHandler(this.toolStripStatusLabel2_Click);
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.minimizeToolStripMenuItem,
            this.logOffToolStripMenuItem});
            this.menuToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478369728_Menu_icon;
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(78, 25);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // minimizeToolStripMenuItem
            // 
            this.minimizeToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478369431_minus_circle;
            this.minimizeToolStripMenuItem.Name = "minimizeToolStripMenuItem";
            this.minimizeToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.minimizeToolStripMenuItem.Text = "Minimize";
            this.minimizeToolStripMenuItem.Click += new System.EventHandler(this.minimizeToolStripMenuItem_Click);
            // 
            // logOffToolStripMenuItem
            // 
            this.logOffToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478369627_SignOut;
            this.logOffToolStripMenuItem.Name = "logOffToolStripMenuItem";
            this.logOffToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.logOffToolStripMenuItem.Text = "Log Off";
            this.logOffToolStripMenuItem.Click += new System.EventHandler(this.logOffToolStripMenuItem_Click);
            // 
            // myDashboardToolStripMenuItem
            // 
            this.myDashboardToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unAssignedBugToolStripMenuItem,
            this.assignedBugsToolStripMenuItem});
            this.myDashboardToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478369790_house;
            this.myDashboardToolStripMenuItem.Name = "myDashboardToolStripMenuItem";
            this.myDashboardToolStripMenuItem.Size = new System.Drawing.Size(140, 25);
            this.myDashboardToolStripMenuItem.Text = "My Dashboard";
            this.myDashboardToolStripMenuItem.Click += new System.EventHandler(this.myDashboardToolStripMenuItem_Click);
            // 
            // unAssignedBugToolStripMenuItem
            // 
            this.unAssignedBugToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478370098_add_book_cover_reading_task;
            this.unAssignedBugToolStripMenuItem.Name = "unAssignedBugToolStripMenuItem";
            this.unAssignedBugToolStripMenuItem.Size = new System.Drawing.Size(199, 26);
            this.unAssignedBugToolStripMenuItem.Text = "Unassigned Bugs";
            this.unAssignedBugToolStripMenuItem.Click += new System.EventHandler(this.unAssignedBugToolStripMenuItem_Click);
            // 
            // assignedBugsToolStripMenuItem
            // 
            this.assignedBugsToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478369995_31_Task;
            this.assignedBugsToolStripMenuItem.Name = "assignedBugsToolStripMenuItem";
            this.assignedBugsToolStripMenuItem.Size = new System.Drawing.Size(199, 26);
            this.assignedBugsToolStripMenuItem.Text = "Assigned Bugs";
            this.assignedBugsToolStripMenuItem.Click += new System.EventHandler(this.assignedBugsToolStripMenuItem_Click);
            // 
            // newBugToolStripMenuItem
            // 
            this.newBugToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478369828_new_24;
            this.newBugToolStripMenuItem.Name = "newBugToolStripMenuItem";
            this.newBugToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newBugToolStripMenuItem.Size = new System.Drawing.Size(129, 25);
            this.newBugToolStripMenuItem.Text = "File New Bug";
            this.newBugToolStripMenuItem.Click += new System.EventHandler(this.newBugToolStripMenuItem_Click);
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versionToolStripMenuItem,
            this.platformToolStripMenuItem,
            this.userToolStripMenuItem,
            this.projectToolStripMenuItem1,
            this.bugToolStripMenuItem1,
            this.componentToolStripMenuItem});
            this.settingToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478369871_Settings_2;
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(87, 25);
            this.settingToolStripMenuItem.Text = "Setting";
            // 
            // versionToolStripMenuItem
            // 
            this.versionToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478370170_versions;
            this.versionToolStripMenuItem.Name = "versionToolStripMenuItem";
            this.versionToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.versionToolStripMenuItem.Text = "Version";
            this.versionToolStripMenuItem.Click += new System.EventHandler(this.versionToolStripMenuItem_Click);
            // 
            // platformToolStripMenuItem
            // 
            this.platformToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478370240_Posterous;
            this.platformToolStripMenuItem.Name = "platformToolStripMenuItem";
            this.platformToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.platformToolStripMenuItem.Text = "Platform";
            this.platformToolStripMenuItem.Click += new System.EventHandler(this.platformToolStripMenuItem_Click);
            // 
            // userToolStripMenuItem
            // 
            this.userToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478370275_Profile01;
            this.userToolStripMenuItem.Name = "userToolStripMenuItem";
            this.userToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.userToolStripMenuItem.Text = "User";
            this.userToolStripMenuItem.Click += new System.EventHandler(this.userToolStripMenuItem_Click);
            // 
            // projectToolStripMenuItem1
            // 
            this.projectToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.projectAssignmentToolStripMenuItem});
            this.projectToolStripMenuItem1.Image = global::BugTracker.Properties.Resources._1478370307_free_11;
            this.projectToolStripMenuItem1.Name = "projectToolStripMenuItem1";
            this.projectToolStripMenuItem1.Size = new System.Drawing.Size(162, 26);
            this.projectToolStripMenuItem1.Text = "Project";
            this.projectToolStripMenuItem1.Click += new System.EventHandler(this.projectToolStripMenuItem1_Click);
            // 
            // projectAssignmentToolStripMenuItem
            // 
            this.projectAssignmentToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478369995_31_Task;
            this.projectAssignmentToolStripMenuItem.Name = "projectAssignmentToolStripMenuItem";
            this.projectAssignmentToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.projectAssignmentToolStripMenuItem.Text = "Project Assignment";
            this.projectAssignmentToolStripMenuItem.Click += new System.EventHandler(this.projectAssignmentToolStripMenuItem_Click);
            // 
            // bugToolStripMenuItem1
            // 
            this.bugToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bugAssignmentToolStripMenuItem});
            this.bugToolStripMenuItem1.Image = global::BugTracker.Properties.Resources.Bug;
            this.bugToolStripMenuItem1.Name = "bugToolStripMenuItem1";
            this.bugToolStripMenuItem1.Size = new System.Drawing.Size(162, 26);
            this.bugToolStripMenuItem1.Text = "Bug";
            this.bugToolStripMenuItem1.Click += new System.EventHandler(this.bugToolStripMenuItem1_Click);
            // 
            // bugAssignmentToolStripMenuItem
            // 
            this.bugAssignmentToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478369995_31_Task;
            this.bugAssignmentToolStripMenuItem.Name = "bugAssignmentToolStripMenuItem";
            this.bugAssignmentToolStripMenuItem.Size = new System.Drawing.Size(193, 26);
            this.bugAssignmentToolStripMenuItem.Text = "Bug Assignment";
            this.bugAssignmentToolStripMenuItem.Click += new System.EventHandler(this.bugAssignmentToolStripMenuItem_Click);
            // 
            // componentToolStripMenuItem
            // 
            this.componentToolStripMenuItem.Image = global::BugTracker.Properties.Resources._1478370372_service_07;
            this.componentToolStripMenuItem.Name = "componentToolStripMenuItem";
            this.componentToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.componentToolStripMenuItem.Text = "Component";
            this.componentToolStripMenuItem.Click += new System.EventHandler(this.componentToolStripMenuItem_Click);
            // 
            // BugTrackerMDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1213, 447);
            this.ControlBox = false;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "BugTrackerMDI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.BugTrackerMDI_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripMainUsername;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripMenuItem newBugToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusUser;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem platformToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem projectAssignmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem myDashboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bugToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bugAssignmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem componentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unAssignedBugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assignedBugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOffToolStripMenuItem;
    }
}