﻿using BugTrackerBL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmBugAssignedToMe : Form
    {
        BugBL bugBl = null;
        public FrmBugAssignedToMe()
        {
            InitializeComponent();
            bugBl = new BugBL();
            GetAssignedBugs();
        }
        private static FrmBugAssignedToMe BugListAdmin = null;
        public static FrmBugAssignedToMe Instance()
        {
            if (BugListAdmin == null)
            {
                BugListAdmin = new FrmBugAssignedToMe();
            }
            return BugListAdmin;
        }

        private void FrmBugListAdmin_FormClosing(object sender, FormClosingEventArgs e)
        {
            BugListAdmin = null;
        }
        private void GetAssignedBugs()
        {
            var AssignedList = bugBl.BugAssignedToMe();
            dgvAssignedBugsList.DataSource = null;
            dgvAssignedBugsList.DataSource = AssignedList;

        }

        private void txtBugSearch_TextChanged(object sender, EventArgs e)
        {
            var AssignedList = bugBl.AssignedBugList(txtBugSearch.Text.Trim());
            dgvAssignedBugsList.DataSource = null;
            dgvAssignedBugsList.DataSource = AssignedList;

        }

        private void dgvAssignedBugsList_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int Index = e.RowIndex;
            int bugId = int.Parse(dgvAssignedBugsList.Rows[Index].Cells[0].Value.ToString());
            var bugDetail = bugBl.BugDetail(bugId);
            FrmBugDetails _bugDetail = FrmBugDetails.Instance();
            _bugDetail.MdiParent = this.ParentForm;
            FrmBugDetails.dataModel = bugDetail;
            _bugDetail.Show();
        }
    }
}
