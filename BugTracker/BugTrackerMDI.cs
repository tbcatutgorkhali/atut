﻿using BugTrackerBL.Models;
using System;
using System.Windows.Forms;


//Dashboard for admin
namespace BugTracker
{
    public partial class BugTrackerMDI : Form
    {
        //variable to hold active user info
        private string LoggedUser;
        private string LoggedUserType;
        public BugTrackerMDI()
        {
            InitializeComponent();
            this.LoggedUser = ActiveUser.Username ?? "Test User";
            toolStripStatusUser.Text = LoggedUser;
            LoggedUserType = ActiveUser.UserType ?? "Admin";
            ActiveUser.LoginId = ActiveUser.LoginId != 0 ? ActiveUser.LoginId : 1;
            if (!string.Equals(LoggedUserType, "Admin"))
            {
                settingToolStripMenuItem.Enabled = false;
                settingToolStripMenuItem.Visible = false;
            }
        }

        //exit application
        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //file new bug menu click event
        private void newBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmNewBug _newBug = FrmNewBug.Instance();
            _newBug.MdiParent = this;
            _newBug.Show();
        }
        //form load event
        private void BugTrackerMDI_Load(object sender, EventArgs e)
        {
            FrmBugListUnAssigned _bugUnassigned = FrmBugListUnAssigned.Instance();
            _bugUnassigned.MdiParent = this;
            _bugUnassigned.Show();
            FrmBugListAssigned _bugassigned = FrmBugListAssigned.Instance();
            _bugassigned.MdiParent = this;
            _bugassigned.Show();
        }
        //severity menu click event
        //private void severityToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    FrmSeverity _severity = FrmSeverity.Instance();
        //    _severity.MdiParent = this;
        //    _severity.Show();
        //}
        private void platformToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPlatform _platform = FrmPlatform.Instance();
            _platform.MdiParent = this;
            _platform.Show();
        }
        private void versionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmVersion _version = FrmVersion.Instance();
            _version.MdiParent = this;
            _version.Show();
        }

        //private void statusToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    FrmStatus _status = FrmStatus.Instance();
        //    _status.MdiParent = this;
        //    _status.Show();
        //}

        private void projectToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmProject _project = FrmProject.Instance();
            _project.MdiParent = this;
            _project.Show();
        }

        private void projectAssignmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmProjecAccess _projecAccess = FrmProjecAccess.Instance();
            _projecAccess.MdiParent = this;
            _projecAccess.Show();
        }

        //private void userTypeToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    FrmUserType _userType = FrmUserType.Instance();
        //    _userType.MdiParent = this;
        //    _userType.Show();
        //}

        private void userToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmUser _user = FrmUser.Instance();
            _user.MdiParent = this;
            _user.Show();
        }

        private void bugToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void componentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmComponent _component = FrmComponent.Instance();
            _component.MdiParent = this;
            _component.Show();
        }

        private void myDashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void unAssignedBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmBugListUnAssigned _bugUnassigned = FrmBugListUnAssigned.Instance();
            _bugUnassigned.MdiParent = this;
            _bugUnassigned.Show();
        }

        private void assignedBugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmBugListAssigned _bugassigned = FrmBugListAssigned.Instance();
            _bugassigned.MdiParent = this;
            _bugassigned.Show();
        }

        private void minimizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void bugAssignmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmBugAssignment _bugAssignment = FrmBugAssignment.Instance();
            _bugAssignment.MdiParent = this;
            _bugAssignment.Show();
        }

        private void logOffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActiveUser.Username = "";
            ActiveUser.UserType = "";
            ActiveUser.UserTypeId = 0;
            ActiveUser.LoginId = 0;
            FrmLogin Login = new FrmLogin();
            Login.Show();
            this.Hide();
        }
    }
}
