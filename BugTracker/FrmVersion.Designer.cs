﻿namespace BugTracker
{
    partial class FrmVersion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVersion));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSaveVersion = new System.Windows.Forms.Button();
            this.txtVersionDescription = new System.Windows.Forms.TextBox();
            this.txtVersionTitle = new System.Windows.Forms.TextBox();
            this.txtVersionId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewVersion = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVersion)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnSaveVersion);
            this.panel1.Controls.Add(this.txtVersionDescription);
            this.panel1.Controls.Add(this.txtVersionTitle);
            this.panel1.Controls.Add(this.txtVersionId);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 178);
            this.panel1.TabIndex = 0;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnDelete.Location = new System.Drawing.Point(333, 107);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 33);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSaveVersion
            // 
            this.btnSaveVersion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnSaveVersion.Location = new System.Drawing.Point(252, 107);
            this.btnSaveVersion.Name = "btnSaveVersion";
            this.btnSaveVersion.Size = new System.Drawing.Size(75, 33);
            this.btnSaveVersion.TabIndex = 6;
            this.btnSaveVersion.Text = "Save";
            this.btnSaveVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSaveVersion.UseVisualStyleBackColor = true;
            this.btnSaveVersion.Click += new System.EventHandler(this.btnSaveVersion_Click);
            // 
            // txtVersionDescription
            // 
            this.txtVersionDescription.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtVersionDescription.Location = new System.Drawing.Point(252, 72);
            this.txtVersionDescription.Name = "txtVersionDescription";
            this.txtVersionDescription.Size = new System.Drawing.Size(231, 29);
            this.txtVersionDescription.TabIndex = 5;
            // 
            // txtVersionTitle
            // 
            this.txtVersionTitle.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtVersionTitle.Location = new System.Drawing.Point(252, 37);
            this.txtVersionTitle.Name = "txtVersionTitle";
            this.txtVersionTitle.Size = new System.Drawing.Size(231, 29);
            this.txtVersionTitle.TabIndex = 4;
            // 
            // txtVersionId
            // 
            this.txtVersionId.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtVersionId.Location = new System.Drawing.Point(96, 2);
            this.txtVersionId.Name = "txtVersionId";
            this.txtVersionId.Size = new System.Drawing.Size(231, 29);
            this.txtVersionId.TabIndex = 3;
            this.txtVersionId.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label3.Location = new System.Drawing.Point(97, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Version Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label2.Location = new System.Drawing.Point(147, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Version Title";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(11, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "VersionId";
            this.label1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.dataGridViewVersion);
            this.panel2.Location = new System.Drawing.Point(12, 197);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(581, 224);
            this.panel2.TabIndex = 1;
            // 
            // dataGridViewVersion
            // 
            this.dataGridViewVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewVersion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewVersion.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridViewVersion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewVersion.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewVersion.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewVersion.MultiSelect = false;
            this.dataGridViewVersion.Name = "dataGridViewVersion";
            this.dataGridViewVersion.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewVersion.Size = new System.Drawing.Size(575, 218);
            this.dataGridViewVersion.TabIndex = 0;
            this.dataGridViewVersion.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewVersion_RowHeaderMouseClick);
            // 
            // FrmVersion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 433);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmVersion";
            this.Text = "Project Versions";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmVersion_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVersion)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtVersionDescription;
        private System.Windows.Forms.TextBox txtVersionTitle;
        private System.Windows.Forms.TextBox txtVersionId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveVersion;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewVersion;
        private System.Windows.Forms.Button btnDelete;
    }
}