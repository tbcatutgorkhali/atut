﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmUserType : Form
    {
        UserTypeBL UserTypeBl = null;
        public FrmUserType()
        {
            InitializeComponent();
            UserTypeBl = new UserTypeBL();
            GetUserTypeList();
        }
        private static FrmUserType UserType = null;
        public static FrmUserType Instance()
        {
            if (UserType == null)
            {
                UserType = new FrmUserType();
            }
            return UserType;
        }

        private void FrmUserType_FormClosed(object sender, FormClosedEventArgs e)
        {
            UserType = null;
        }

        private void GetUserTypeList()
        {
            var UserTypeList = UserTypeBl.List();
            if (UserTypeList.Any())
            {
                dataGridViewUserType.DataSource = null;
                dataGridViewUserType.DataSource = UserTypeList.ToList();
            }
        }

        private void btnSaveUserType_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUserTypeTitle.Text))
            {
                return;
            }
            var model = new UserTypeModel();
            int id = 0;
            int.TryParse(txtUserTypeId.Text.Trim().ToString(), out id);
            model.UserTypeId = id;
            model.Title = txtUserTypeTitle.Text.Trim().ToString();
            if (id == 0)
            {
                var createResult = UserTypeBl.Create(model);
                if (createResult)
                {
                    GetUserTypeList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                var editResult = UserTypeBl.Edit(model);
                if (editResult)
                {
                    GetUserTypeList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
        private void ResetForm()
        {
            txtUserTypeId.Text = "";
            txtUserTypeTitle.Text = "";
            btnDelete.Visible = false;
        }

        private void dataGridViewUserType_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            var model = new UserTypeModel();
            model.UserTypeId = int.Parse(dataGridViewUserType.Rows[index].Cells[0].Value.ToString());
            model.Title = dataGridViewUserType.Rows[index].Cells[1].Value.ToString();
            txtUserTypeId.Text = model.UserTypeId.ToString();
            txtUserTypeTitle.Text = model.Title;
            btnDelete.Visible = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int UserTypeId = int.Parse(txtUserTypeId.Text.Trim());
            var deleteResult = UserTypeBl.Delete(UserTypeId);
            if (deleteResult)
            {
                GetUserTypeList();
                ResetForm();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }
    }
}
