﻿using BugTrackerBL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmBugListUnAssigned : Form
    {
        BugBL bugBl = null;
        public FrmBugListUnAssigned()
        {
            InitializeComponent();
            bugBl = new BugBL();
            GetUnAssignedBugs();
        }
        private static FrmBugListUnAssigned BugListAdmin = null;
        public static FrmBugListUnAssigned Instance()
        {
            if (BugListAdmin == null)
            {
                BugListAdmin = new FrmBugListUnAssigned();
            }
            return BugListAdmin;
        }

        private void FrmBugListAdmin_FormClosing(object sender, FormClosingEventArgs e)
        {
            BugListAdmin = null;
        }
        private void GetUnAssignedBugs()
        {
            var unAssignedList = bugBl.UnassginedList();
            dgvUnassignedBugsList.DataSource = null;
            dgvUnassignedBugsList.DataSource = unAssignedList;

        }

        private void txtBugSearch_TextChanged(object sender, EventArgs e)
        {
            var unAssignedList = bugBl.UnassginedList(txtBugSearch.Text.Trim());
            dgvUnassignedBugsList.DataSource = null;
            dgvUnassignedBugsList.DataSource = unAssignedList;


        }

        private void dgvUnassignedBugsList_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int Index = e.RowIndex;
            int bugId = int.Parse(dgvUnassignedBugsList.Rows[Index].Cells[0].Value.ToString());
            var bugDetail = bugBl.BugDetail(bugId);
            FrmBugDetails _bugDetail = FrmBugDetails.Instance();
            _bugDetail.MdiParent = this.ParentForm;
            FrmBugDetails.dataModel = bugDetail;
            _bugDetail.Show();
        }
    }
}
