﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmNewBug : Form
    {
        private ProjectBL projectBl = null;
        private PlatformBL platformBl = null;
        private VersionBL versionBl = null;
        private ComponentBL componentBl = null;
        private AttachmentBL attachmentBl = null;
        private SeverityBL severityBl = null;
        private BugBL bugBL = null;
        private SharpBucketBL sharpBucket = null;

        public FrmNewBug()
        {
            InitializeComponent();
            projectBl = new ProjectBL();
            platformBl = new PlatformBL();
            versionBl = new VersionBL();
            componentBl = new ComponentBL();
            attachmentBl = new AttachmentBL();
            severityBl = new SeverityBL();
            bugBL = new BugBL();

            GetProjecList();
            GetPlatformList();
            GetVersionList();
            GetComponentList();
            GetSeverityList();
            ClearUI();
        }
        private static FrmNewBug newBug = null;
        public static FrmNewBug Instance()
        {
            if (newBug == null)
            {
                newBug = new FrmNewBug();
            }
            return newBug;
        }

        private void FrmNewBug_FormClosed(object sender, FormClosedEventArgs e)
        {
            newBug = null;
        }

        private void btnAttachImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog ImageDialog = new OpenFileDialog();
            ImageDialog.Filter = "Image Files|*.jpg;*.png;*.gif";
            if (ImageDialog.ShowDialog() == DialogResult.OK && ImageDialog.FileName.Length > 0)
            {
                PicBoxAttachment.Image = Image.FromFile(ImageDialog.FileName);
            }
        }
        private void GetProjecList()
        {
            var projectList = projectBl.List();
            if (projectList.Any())
            {
                cmbProductId.DataSource = projectList.ToList();
                cmbProductId.DisplayMember = "Title";
                cmbProductId.ValueMember = "ProjectId";
            }
        }
        private void GetSeverityList()
        {
            var projectList = projectBl.List();
            if (projectList.Any())
            {
                cmbSeverity.DataSource = severityBl.List();
                cmbSeverity.DisplayMember = "Title";
                cmbSeverity.ValueMember = "SeverityId";
            }
        }

        private void GetComponentList()
        {
            var projectList = projectBl.List();
            if (projectList.Any())
            {
                cmbComponentId.DataSource = componentBl.List();
                cmbComponentId.DisplayMember = "Title";
                cmbComponentId.ValueMember = "ComponentId";
            }
        }
        private void GetPlatformList()
        {
            var projectList = projectBl.List();
            if (projectList.Any())
            {
                cmbPlatformId.DataSource = platformBl.List();
                cmbPlatformId.DisplayMember = "Title";
                cmbPlatformId.ValueMember = "PlatformId";
            }
        }
        private void GetVersionList()
        {
            var projectList = versionBl.List();
            if (projectList.Any())
            {
                cmbVersionId.DataSource = versionBl.List();
                cmbVersionId.DisplayMember = "Title";
                cmbVersionId.ValueMember = "VersionId";
            }
        }
        public static Image byteToImage(byte[] byt)
        {
            MemoryStream MSImage = new MemoryStream(byt);
            Image ReturnImage = Image.FromStream(MSImage);
            return ReturnImage;
        }
        public static byte[] ImageToByte(Image image)
        {
            MemoryStream MS = new MemoryStream();
            image.Save(MS, System.Drawing.Imaging.ImageFormat.Jpeg);
            return MS.ToArray();

        }

        private void btnBugSubmit_Click(object sender, EventArgs e)
        {
            var bugModel = new BugModel();
            var attachmentModel = new AttachmentModel();

            if (PicBoxAttachment.Image != null || txtAttachmentText.Text != "")
            {
                if (PicBoxAttachment.Image != null)
                {
                    attachmentModel.AttachmentFileName = ImageToByte(PicBoxAttachment.Image);
                }
                attachmentModel.AttachementText = txtAttachmentText.Text.Trim();
            }
            int attachmentId = 0;
            attachmentBl.Create(attachmentModel, out attachmentId);
            bugModel.AttachmentId = attachmentId;
            bugModel.ProjectId = int.Parse(cmbProductId.SelectedValue.ToString());
            bugModel.ComponentId = int.Parse(cmbComponentId.SelectedValue.ToString());
            bugModel.VersionId = int.Parse(cmbVersionId.SelectedValue.ToString());
            bugModel.SeverityId = int.Parse(cmbSeverity.SelectedValue.ToString());
            bugModel.PlatformId = int.Parse(cmbPlatformId.SelectedValue.ToString());
            bugModel.Sumary = txtSummary.Text.Trim();
            bugModel.Description = txtDescription.Text.Trim();
            bugModel.ReporterId = ActiveUser.LoginId;
            int bugId;
            var bugCreateResult = bugBL.Create(bugModel, out bugId);
            bugModel.BugId = bugId;
            if (bugCreateResult)
            {
                sharpBucket = new SharpBucketBL();
                int IssueId;
                bool result = sharpBucket.AddNewIssue(bugModel, out IssueId);
                if (result)
                {
                    bugModel.VersionControlIssueId = IssueId;
                    if (bugBL.SetVersionControlId(bugModel.BugId, bugModel.VersionControlIssueId ?? 0))
                    {
                        MessageBox.Show("Bug Reported Successfully, Admin will approve this bug after review.", "Success");
                        ClearUI();
                    }
                    else
                    {
                        MessageBox.Show("Bug submission failed, Try again.", "Failure");
                    }
                }
                else
                {
                    MessageBox.Show("Bug Reported Successfully, But issue is not reported to version control.", "Success");
                    ClearUI();
                }
            }


        }
        private void ClearUI()
        {
            txtBugId.Text = "";
            txtDescription.Text = "";
            txtSummary.Text = "";
            txtAttachmentText.Text = "";
            cmbComponentId.SelectedIndex = -1;
            cmbPlatformId.SelectedIndex = -1;
            cmbProductId.SelectedIndex = -1;
            cmbSeverity.SelectedIndex = -1;
            cmbVersionId.SelectedIndex = -1;
            PicBoxAttachment.Image = null;
        }
    }
}
