﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmComponent : Form
    {
        ComponentBL ComponentBl = null;
        public FrmComponent()
        {
            InitializeComponent();
            ComponentBl = new ComponentBL();
            GetComponentList();
        }
        private static FrmComponent Component = null;
        public static FrmComponent Instance()
        {
            if (Component == null)
            {
                Component = new FrmComponent();
            }
            return Component;
        }

        private void FrmComponent_FormClosed(object sender, FormClosedEventArgs e)
        {
            Component = null;
        }

        private void GetComponentList()
        {
            var ComponentList = ComponentBl.List();
            if (ComponentList.Any())
            {
                dataGridViewComponent.DataSource = null;
                dataGridViewComponent.DataSource = ComponentList.ToList();
            }
        }

        private void btnSaveComponent_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtComponentTitle.Text))
            {
                return;
            }
            var model = new ComponentModel();
            int id = 0;
            int.TryParse(txtComponentId.Text.Trim().ToString(), out id);
            model.ComponentId = id;
            model.Title = txtComponentTitle.Text.Trim().ToString();
            model.Description = txtComponentDescription.Text.Trim().ToString();
            model.CreatedBy = ActiveUser.LoginId;
            if (id == 0)
            {
                var createResult = ComponentBl.Create(model);
                if (createResult)
                {
                    GetComponentList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                var editResult = ComponentBl.Edit(model);
                if (editResult)
                {
                    GetComponentList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
        private void ResetForm()
        {
            txtComponentId.Text = "";
            txtComponentDescription.Text = "";
            txtComponentTitle.Text = "";
            btnDelete.Visible = false;
        }

        private void dataGridViewComponent_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            var model = new ComponentModel();
            model.ComponentId = int.Parse(dataGridViewComponent.Rows[index].Cells[0].Value.ToString());
            model.Title = dataGridViewComponent.Rows[index].Cells[1].Value.ToString();
            model.Description = dataGridViewComponent.Rows[index].Cells[2].Value.ToString();
            txtComponentId.Text = model.ComponentId.ToString();
            txtComponentTitle.Text = model.Title;
            txtComponentDescription.Text = model.Description;
            btnDelete.Visible = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int ComponentId = int.Parse(txtComponentId.Text.Trim());
            var deleteResult = ComponentBl.Delete(ComponentId);
            if (deleteResult)
            {
                GetComponentList();
                ResetForm();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }
    }
}
