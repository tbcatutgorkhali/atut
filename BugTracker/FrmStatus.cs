﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmStatus : Form
    {
        StatusBL StatusBl = null;
        public FrmStatus()
        {
            InitializeComponent();
            StatusBl = new StatusBL();
            GetStatusList();
        }
        private static FrmStatus Status = null;
        public static FrmStatus Instance()
        {
            if (Status == null)
            {
                Status = new FrmStatus();
            }
            return Status;
        }

        private void FrmStatus_FormClosed(object sender, FormClosedEventArgs e)
        {
            Status = null;
        }

        private void GetStatusList()
        {
            var StatusList = StatusBl.List();
            if (StatusList.Any())
            {
                dataGridViewStatus.DataSource = null;
                dataGridViewStatus.DataSource = StatusList.ToList();
            }
        }

        private void btnSaveStatus_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtStatusTitle.Text))
            {
                return;
            }
            var model = new StatusModel();
            int id = 0;
            int.TryParse(txtStatusId.Text.Trim().ToString(), out id);
            model.StatusId = id;
            model.Title = txtStatusTitle.Text.Trim().ToString();
            model.Description = txtStatusDescription.Text.Trim().ToString();
            if (id == 0)
            {
                var createResult = StatusBl.Create(model);
                if (createResult)
                {
                    GetStatusList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                var editResult = StatusBl.Edit(model);
                if (editResult)
                {
                    GetStatusList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
        private void ResetForm()
        {
            txtStatusId.Text = "";
            txtStatusDescription.Text = "";
            txtStatusTitle.Text = "";
            btnDelete.Visible = false;
        }

        private void dataGridViewStatus_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            var model = new StatusModel();
            model.StatusId = int.Parse(dataGridViewStatus.Rows[index].Cells[0].Value.ToString());
            model.Title = dataGridViewStatus.Rows[index].Cells[1].Value.ToString();
            model.Description = dataGridViewStatus.Rows[index].Cells[2].Value.ToString();
            txtStatusId.Text = model.StatusId.ToString();
            txtStatusTitle.Text = model.Title;
            txtStatusDescription.Text = model.Description;
            btnDelete.Visible = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int StatusId = int.Parse(txtStatusId.Text.Trim());
            var deleteResult = StatusBl.Delete(StatusId);
            if (deleteResult)
            {
                GetStatusList();
                ResetForm();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }
    }
}
