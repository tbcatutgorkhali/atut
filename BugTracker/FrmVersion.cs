﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmVersion : Form
    {
        VersionBL VersionBl = null;
        public FrmVersion()
        {
            InitializeComponent();
            VersionBl = new VersionBL();
            GetVersionList();
        }
        private static FrmVersion Version = null;
        public static FrmVersion Instance()
        {
            if (Version == null)
            {
                Version = new FrmVersion();
            }
            return Version;
        }

        private void FrmVersion_FormClosed(object sender, FormClosedEventArgs e)
        {
            Version = null;
        }

        private void GetVersionList()
        {
            var VersionList = VersionBl.List();
            if (VersionList.Any())
            {
                dataGridViewVersion.DataSource = null;
                dataGridViewVersion.DataSource = VersionList.ToList();
            }
        }

        private void btnSaveVersion_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtVersionTitle.Text))
            {
                return;
            }
            var model = new VersionModel();
            int id = 0;
            int.TryParse(txtVersionId.Text.Trim().ToString(), out id);
            model.VersionId = id;
            model.Title = txtVersionTitle.Text.Trim().ToString();
            model.Description = txtVersionDescription.Text.Trim().ToString();
            if (id == 0)
            {
                var createResult = VersionBl.Create(model);
                if (createResult)
                {
                    GetVersionList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                var editResult = VersionBl.Edit(model);
                if (editResult)
                {
                    GetVersionList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
        private void ResetForm()
        {
            txtVersionId.Text = "";
            txtVersionDescription.Text = "";
            txtVersionTitle.Text = "";
            btnDelete.Visible = false;
        }

        private void dataGridViewVersion_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            var model = new VersionModel();
            model.VersionId = int.Parse(dataGridViewVersion.Rows[index].Cells[0].Value.ToString());
            model.Title = dataGridViewVersion.Rows[index].Cells[1].Value.ToString();
            model.Description = dataGridViewVersion.Rows[index].Cells[2].Value.ToString();
            txtVersionId.Text = model.VersionId.ToString();
            txtVersionTitle.Text = model.Title;
            txtVersionDescription.Text = model.Description;
            btnDelete.Visible = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int VersionId = int.Parse(txtVersionId.Text.Trim());
            var deleteResult = VersionBl.Delete(VersionId);
            if (deleteResult)
            {
                GetVersionList();
                ResetForm();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }
    }
}
