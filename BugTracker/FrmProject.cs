﻿using BugTrackerBL;
using BugTrackerBL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker
{
    public partial class FrmProject : Form
    {
        ProjectBL ProjectBl = null;
        public FrmProject()
        {
            InitializeComponent();
            ProjectBl = new ProjectBL();
            GetProjectList();
        }
        private static FrmProject Project = null;
        public static FrmProject Instance()
        {
            if (Project == null)
            {
                Project = new FrmProject();
            }
            return Project;
        }

        private void FrmProject_FormClosed(object sender, FormClosedEventArgs e)
        {
            Project = null;
        }

        private void GetProjectList()
        {
            var ProjectList = ProjectBl.List();
            if (ProjectList.Any())
            {
                dataGridViewProject.DataSource = null;
                dataGridViewProject.DataSource = ProjectList.ToList();
            }
        }

        private void btnSaveProject_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtProjectTitle.Text))
            {
                return;
            }
            var model = new ProjectModel();
            int id = 0;
            int.TryParse(txtProjectId.Text.Trim().ToString(), out id);
            model.ProjectId = id;
            model.Title = txtProjectTitle.Text.Trim().ToString();
            model.Description = txtProjectDescription.Text.Trim().ToString();
            if (id == 0)
            {
                var createResult = ProjectBl.Create(model);
                if (createResult)
                {
                    GetProjectList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
            else
            {
                var editResult = ProjectBl.Edit(model);
                if (editResult)
                {
                    GetProjectList();
                    ResetForm();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
        }
        private void ResetForm()
        {
            txtProjectId.Text = "";
            txtProjectDescription.Text = "";
            txtProjectTitle.Text = "";
            btnDelete.Visible = false;
        }

        private void dataGridViewProject_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = e.RowIndex;
            var model = new ProjectModel();
            model.ProjectId = int.Parse(dataGridViewProject.Rows[index].Cells[0].Value.ToString());
            model.Title = dataGridViewProject.Rows[index].Cells[1].Value.ToString();
            model.Description = dataGridViewProject.Rows[index].Cells[2].Value.ToString();
            txtProjectId.Text = model.ProjectId.ToString();
            txtProjectTitle.Text = model.Title;
            txtProjectDescription.Text = model.Description;
            btnDelete.Visible = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int ProjectId = int.Parse(txtProjectId.Text.Trim());
            var deleteResult = ProjectBl.Delete(ProjectId);
            if (deleteResult)
            {
                GetProjectList();
                ResetForm();
            }
            else
            {
                MessageBox.Show("Failed");
            }
        }
    }
}
