﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;

namespace BugTrackerBL
{
    //business logic for project
    public class ProjectBL
    {
        //add project
        public bool Create(ProjectModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var insertProject = new Project
                    {
                        Title = model.Title,
                        Description = model.Description
                    };
                    ent.Project.Add(insertProject);
                    ent.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }

        //edit a project
        public bool Edit(ProjectModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Project = ent.Project.Find(model.ProjectId);
                    Project.Title = model.Title;
                    Project.Description = model.Description;
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        //delete project
        public bool Delete(int ProjectId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Project = ent.Project.Find(ProjectId);
                    ent.Project.Remove(Project);
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //list all projects
        public List<ProjectModel> List()
        {
            List<ProjectModel> list = new List<ProjectModel>();
            var ent = new BugTrackerDbEntities();
            var ProjectList = ent.Project;
            foreach (var item in ProjectList)
            {
                list.Add(new ProjectModel
                {
                    ProjectId = item.ProjectId,
                    Title = item.Title,
                    Description = item.Description
                });
            }
            return list;
        }
    }
}
