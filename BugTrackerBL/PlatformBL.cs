﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;

namespace BugTrackerBL
{
    //Business logic class for platform
    public class PlatformBL
    {
        //add platform
        public bool Create(PlatformModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var insertPlatform = new Platform
                    {
                        Title = model.Title,
                        Description = model.Description
                    };
                    ent.Platform.Add(insertPlatform);
                    ent.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }
        //modify platform
        public bool Edit(PlatformModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Platform = ent.Platform.Find(model.PlatformId);
                    Platform.Title = model.Title;
                    Platform.Description = model.Description;
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //delete platform
        public bool Delete(int PlatformId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Platform = ent.Platform.Find(PlatformId);
                    ent.Platform.Remove(Platform);
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        //list all platform
        public List<PlatformModel> List()
        {
            List<PlatformModel> list = new List<PlatformModel>();
            var ent = new BugTrackerDbEntities();
            var PlatformList = ent.Platform;
            foreach (var item in PlatformList)
            {
                list.Add(new PlatformModel
                {
                    PlatformId = item.PlatformId,
                    Title = item.Title,
                    Description = item.Description
                });
            }
            return list;
        }
    }
}
