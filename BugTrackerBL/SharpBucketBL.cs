﻿using System.IO;
using SharpBucket.V1;
using SharpBucket.V1.Pocos;
using Comment = SharpBucket.V1.Pocos.Comment;
using BugTrackerBL.Models;
using System;
using BugTrackerDL;
using System.Linq;

namespace BugTrackerBL
{
    public class SharpBucketBL
    {
        private static string email;
        private static string password;
        private static string consumerKey;
        private static string consumerSecretKey;
        private static string accountName;
        private static string repository;

        public bool AddNewIssue(BugModel bug, out int issueId)
        {
            var sharpBucket = new SharpBucketV1();
            // Decide on which authentication you wish to use
            BasicAuthentication();
            sharpBucket.BasicAuthentication(email, password);

            // ReadTestDataOauth();
            // Two legged OAuth, just supply the consumerKey and the consumerSecretKey and you are done
            // sharpBucket.OAuth2LeggedAuthentication(consumerKey, consumerSecretKey);

            // Three legged OAuth. We can supply our own callback url to which bitbucket will send our pin
            // If we use "oob" as the callback url we will get the bitbuckets url address which will have our pin
            //var authenticator = sharpBucket.OAuth3LeggedAuthentication(consumerKey, consumerSecretKey, "oob");
            //var uri = authenticator.StartAuthentication();
            //Process.Start(uri);
            //var pin = Console.ReadLine();
            // we can now do the final step by using the pin to get our access tokens
            //authenticator.AuthenticateWithPin(pin);

            // of if you saved the tokens you can simply use those
            // var authenticator = sharpBucket.OAuth3LeggedAuthentication(consumerKey, consumerSecretKey, "oauthtoken", "oauthtokensecret");
            UserEndPoint(sharpBucket);
            //int issueId;
            if (AddIssue(sharpBucket, bug, out issueId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool AddNewIssueComment(int IssueId, BugCommentsModel model)
        {
            var sharpBucket = new SharpBucketV1();
            // Decide on which authentication you wish to use
            BasicAuthentication();
            sharpBucket.BasicAuthentication(email, password);

            // ReadTestDataOauth();
            // Two legged OAuth, just supply the consumerKey and the consumerSecretKey and you are done
            // sharpBucket.OAuth2LeggedAuthentication(consumerKey, consumerSecretKey);

            // Three legged OAuth. We can supply our own callback url to which bitbucket will send our pin
            // If we use "oob" as the callback url we will get the bitbuckets url address which will have our pin
            //var authenticator = sharpBucket.OAuth3LeggedAuthentication(consumerKey, consumerSecretKey, "oob");
            //var uri = authenticator.StartAuthentication();
            //Process.Start(uri);
            //var pin = Console.ReadLine();
            // we can now do the final step by using the pin to get our access tokens
            //authenticator.AuthenticateWithPin(pin);

            // of if you saved the tokens you can simply use those
            // var authenticator = sharpBucket.OAuth3LeggedAuthentication(consumerKey, consumerSecretKey, "oauthtoken", "oauthtokensecret");
            int issueId = IssueId;
            UserEndPoint(sharpBucket);
            return AddIssuesComment(issueId, model, sharpBucket);
        }
        private static void BasicAuthentication()
        {
            // Reads test data information from a file, you should structure it like this:
            // By default it reads from c:\
            // Username:yourUsername
            // Password:yourPassword
            // AccountName:yourAccountName
            // Repository:testRepositoryD:\Documents\BugTrackerTBC\BugTracker\Credentials.txt
            //D:\Documents\BugTrackerTBC\BugTracker\Resources\Credentials.txt
            var lines = File.ReadAllLines("c:\\TestInformation.txt");
            email = lines[0].Split(':')[1];
            password = lines[1].Split(':')[1];
            ReadAccoutNameAndRepository(lines);
        }
        private static void ReadAccoutNameAndRepository(string[] lines)
        {
            accountName = lines[2].Split(':')[1];
            repository = lines[3].Split(':')[1];
        }
        private static void UserEndPoint(SharpBucketV1 sharpBucket)
        {
            try
            {
                var userEndPoint = sharpBucket.UserEndPoint();
                var info = userEndPoint.GetInfo();
                var privileges = userEndPoint.ListPrivileges();
                var follows = userEndPoint.ListFollows();
                var userRepos = userEndPoint.ListRepositories();
                var userReposOverview = userEndPoint.RepositoriesOverview();
                var userRepositoryDashboard = userEndPoint.GetRepositoryDasboard();
            }
            catch (Exception)
            {

                return;
            }
        }

        private static bool AddIssue(SharpBucketV1 sharpBucket, BugModel bug, out int IssueId)
        {

            // Issue comments 
            try
            {
                var issuesResource = sharpBucket.RepositoriesEndPoint(accountName, repository).IssuesResource();
                int severityId = bug.SeverityId;
                SeverityEnum severityEnum = (SeverityEnum)severityId;
                string severity = severityEnum.ToString();
                string description = "";
                if (bug.AttachmentId > 0)
                {
                    using (var ent = new BugTrackerDbEntities())
                    {
                        var queryResult = ent.Attachment.FirstOrDefault(x => x.AttachmentId == bug.AttachmentId);
                        description = queryResult.AttachementText;
                    }
                }
                // Issues
                var issues = issuesResource.ListIssues();
                var newIssue = new Issue { title = bug.Sumary, content = description != "" ? ActiveUser.Username.ToString() + " : " + bug.Description + "\n" + description : bug.ReporterId.ToString() + " : " + bug.Description, status = "new", priority = severity, kind = "bug" };
                var newIssueResult = issuesResource.PostIssue(newIssue);
                var issue = issuesResource.GetIssue(newIssueResult.local_id);
                int ISSUE_ID = newIssueResult.local_id ?? 0;
                if (ISSUE_ID > 1)
                {
                    IssueId = ISSUE_ID;
                    return true;
                }
                else
                {
                    IssueId = 0;
                    return false;
                }
            }
            catch (Exception)
            {

                IssueId = 0;
                return false;
            }

        }
        private static bool AddIssuesComment(int IssueId, BugCommentsModel model, SharpBucketV1 sharpBucket)
        {
            // Issues
            try
            {
                int ISSUE_ID = IssueId;
                // Issue comments 
                var issuesResource = sharpBucket.RepositoriesEndPoint(accountName, repository).IssuesResource();
                var issueResource = issuesResource.IssueResource(ISSUE_ID);
                var issueComments = issueResource.ListComments();
                var newComment = new Comment { content = (model.CommentByName + " : \n " + model.Comment).ToString() };
                var newCommentResult = issueResource.PostComment(newComment);
                var comment = issueResource.GetIssueComment(newCommentResult.comment_id);
                int commentId = newCommentResult.comment_id ?? 0;
                if (commentId > 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
