﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BugTrackerBL
{
    public class StatusBL
    {
        //public bool Create(StatusModel model)
        //{
        //    try
        //    {
        //        using (var ent = new BugTrackerDbEntities())
        //        {
        //            var insertStatus = new BugTrackerDL.Status
        //            {
        //                Title = model.Title,
        //                Description = model.Description
        //            };
        //            ent.Status.Add(insertStatus);
        //            ent.SaveChanges();
        //            return true;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }

        //}
        //public bool Edit(StatusModel model)
        //{
        //    try
        //    {
        //        using (var ent = new BugTrackerDbEntities())
        //        {
        //            var Status = ent.Status.Find(model.StatusId);
        //            Status.Title = model.Title;
        //            Status.Description = model.Description;
        //            ent.SaveChanges();
        //        }
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }

        //}
        //public bool Delete(int StatusId)
        //{
        //    try
        //    {
        //        using (var ent = new BugTrackerDbEntities())
        //        {
        //            var Status = ent.Status.Find(StatusId);
        //            ent.Status.Remove(Status);
        //            ent.SaveChanges();
        //        }
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }

        //}

            //list all status types
        public List<StatusModel> List()
        {
            List<StatusModel> list = new List<StatusModel>();
            var ent = new BugTrackerDbEntities();
            IQueryable<Status> StatusList = null;

            switch (ActiveUser.UserType.ToLower())
            {
                case "admin":
                    StatusList = ent.Status.Where(
                        x => x.StatusId != (int)StatusEnum.Resolved
                    && x.StatusId != (int)StatusEnum.Verified);
                    break;
                case "debugger":
                    StatusList = ent.Status.Where(
                        x => x.StatusId != (int)StatusEnum.UnConfirmed
                    && x.StatusId != (int)StatusEnum.New
                    && x.StatusId != (int)StatusEnum.Assigned
                    && x.StatusId != (int)StatusEnum.Verified
                    && x.StatusId != (int)StatusEnum.Closed);
                    break;
                case "tester":
                    StatusList = ent.Status.Where(
                       x => x.StatusId != (int)StatusEnum.UnConfirmed
                   && x.StatusId != (int)StatusEnum.New
                   && x.StatusId != (int)StatusEnum.Assigned
                   && x.StatusId != (int)StatusEnum.Verified
                   && x.StatusId != (int)StatusEnum.Closed
                   && x.StatusId != (int)StatusEnum.Resolved);
                    break;
                case "specialist":
                    StatusList = ent.Status.Where(
                       x => x.StatusId != (int)StatusEnum.UnConfirmed
                   && x.StatusId != (int)StatusEnum.New
                   && x.StatusId != (int)StatusEnum.Assigned
                   && x.StatusId != (int)StatusEnum.Verified
                   && x.StatusId != (int)StatusEnum.Closed);
                    break;
                case "qa":
                    StatusList = ent.Status.Where(
                       x => x.StatusId != (int)StatusEnum.UnConfirmed
                   && x.StatusId != (int)StatusEnum.New
                   && x.StatusId != (int)StatusEnum.Assigned
                   );
                    break;
                default:

                    break;
            }
            foreach (var item in StatusList)
            {
                list.Add(new StatusModel
                {
                    StatusId = item.StatusId,
                    Title = item.Title,
                    Description = item.Description
                });
            }
            return list;
        }
    }
}
