﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BugTrackerDL;
using BugTrackerBL.Models;

namespace BugTrackerBL
{
    //Business logic for login
    public class LoginBL
    {

        //create new user
        public bool Create(LoginModel model)
        {
            var pass = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(model.Password));
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var insertLogin = new Login
                    {
                        UserTypeId = model.UserTypeId,
                        Username = model.Username,
                        Password = pass,
                        IsLocker = model.IsLocker,
                        FullName = model.FullName,
                        Phone = model.Phone,
                        Email = model.Email
                    };
                    ent.Login.Add(insertLogin);
                    ent.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        //edit exisitng user
        public bool Edit(LoginModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Login = ent.Login.Find(model.LoginId);
                    Login.UserTypeId = model.UserTypeId;
                    Login.Username = model.Username;
                    Login.Password = model.Password;
                    Login.IsLocker = model.IsLocker;
                    Login.FullName = model.FullName;
                    Login.Phone = model.Phone;
                    Login.Email = model.Email;
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //check if login credentials are correct and user exists
        public bool CheckLogin(LoginModel loginModel, out string userType)
        {
            var ent = new BugTrackerDbEntities();
            var pass = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(loginModel.Password));
            var user = ent.Login.Where(u => u.Username.Trim().ToUpper() == loginModel.Username.Trim().ToUpper() && u.Password == pass && u.IsLocker == false).FirstOrDefault();
            //if user exists
            if (user != null)
            {
                ActiveUser.LoginId = user.LoginId;
                ActiveUser.UserTypeId = user.UserTypeId;
                ActiveUser.Username = user.Username;
                ActiveUser.UserType = user.UserType.Title;
                userType = ActiveUser.UserType;
                return true;
            }
            // false result if user does not exists
            userType = "";
            return false;
        }
        //unnessessary codes
        //public LoggedInUserModel GetLoggedUser(int loginId)
        //{
        //    LoggedInUserModel userModel = new LoggedInUserModel();
        //    var ent = new BugTrackerDbEntities();
        //    var user = ent.Login.Include("UserType").Where(x => x.LoginId == loginId).FirstOrDefault();
        //    userModel.Username = user.Username;
        //    userModel.UserTypeName = user.UserType.Title;
        //    return userModel;
        //}

            /// <summary>
            /// list of all users
            /// </summary>
            /// <returns></returns>
        public List<LoginModel> List()
        {
            List<LoginModel> list = new List<LoginModel>();
            var ent = new BugTrackerDbEntities();
            var loginList = ent.Login;
            foreach (var item in loginList)
            {
                list.Add(new LoginModel
                {
                    UserTypeId = item.UserTypeId,
                    UserTypeTitle = item.UserType.Title,
                    LoginId = item.LoginId,
                    Username = item.Username,
                    Password = "Not Shown",//IsLocker = item.IsLocker,
                    LockStatus = item.IsLocker == false ? "Active" : "Locked"
                });
            }
            return list;
        }
        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="LoginId"></param>
        /// <returns></returns>
        public bool Delete(int LoginId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Login = ent.Login.Find(LoginId);
                    ent.Login.Remove(Login);
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
