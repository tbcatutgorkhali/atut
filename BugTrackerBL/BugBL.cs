﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BugTrackerBL
{
    //Business logic class for Bugs
    public class BugBL
    {
        //Report new bug
        public bool Create(BugModel model, out int bugId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var insertBug = new Bugs
                    {
                        ReporterId = model.ReporterId,
                        ProjectId = model.ProjectId,
                        ComponentId = model.ComponentId,
                        VersionId = model.VersionId,
                        SeverityId = model.SeverityId,
                        PlatformId = model.PlatformId,
                        AttachmentId = model.AttachmentId,
                        StatusId = 1,
                        Sumary = model.Sumary,
                        Description = model.Description
                    };
                    ent.Bugs.Add(insertBug);
                    ent.SaveChanges();
                    bugId = insertBug.BugId;
                    return true;
                }
            }
            catch (Exception)
            {
                bugId = 0;
                return false;
            }
        }

        //set versioncontrol issue id
        public bool SetVersionControlId(int bugId, int issueId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Bug = ent.Bugs.Find(bugId);
                    Bug.VersionControlIssueId = issueId;
                    ent.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        //change status property of existing bug
        public bool statusChange(int bugId, int statusId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Bug = ent.Bugs.Find(bugId);

                    //if status is New
                    if (statusId == (int)StatusEnum.New)
                    {
                        Bug.StatusId = (int)StatusEnum.New;
                        Bug.ConfirmedBy = ActiveUser.LoginId;
                        Bug.ConfirmedDateTime = DateTime.Now;
                    }
                    // if status is assigned
                    if (statusId == (int)StatusEnum.Assigned)
                    {
                        Bug.StatusId = (int)StatusEnum.Assigned;
                        Bug.ConfirmedBy = ActiveUser.LoginId;
                        Bug.ConfirmedDateTime = DateTime.Now;
                    }
                    // if status is Resolved
                    if (statusId == (int)StatusEnum.Resolved)
                    {
                        Bug.StatusId = (int)StatusEnum.Resolved;
                        Bug.ReSolvedBy = ActiveUser.LoginId;
                        Bug.ReSolvedDateTime = DateTime.Now;
                    }
                    // if status is Reopened
                    if (statusId == (int)StatusEnum.ReOpened)
                    {
                        Bug.StatusId = (int)StatusEnum.ReOpened;
                        Bug.ReOpenedBy = ActiveUser.LoginId;
                        Bug.ReOpenedDateTime = DateTime.Now;
                    }
                    // if status is Verified
                    if (statusId == (int)StatusEnum.Verified)
                    {
                        Bug.StatusId = (int)StatusEnum.Verified;
                        Bug.VerifiedBy = ActiveUser.LoginId;
                        Bug.VerifiedDateTime = DateTime.Now;
                    }
                    // if status is Closed
                    if (statusId == (int)StatusEnum.Closed)
                    {
                        Bug.StatusId = (int)StatusEnum.Closed;
                        Bug.ClosedBy = ActiveUser.LoginId;
                        Bug.ClosedDateTime = DateTime.Now;
                    }
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //Unnecessary codes
        //public bool Delete(int BugId)
        //{
        //    try
        //    {
        //        using (var ent = new BugTrackerDbEntities())
        //        {
        //            var Bug = ent.Bugs.Find(BugId);
        //            ent.Bug.Remove(Bug);
        //            ent.SaveChanges();
        //        }
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        //List un assigned bugs
        public List<BugListModel> UnassginedList(string searchKeyword = "")
        {
            List<BugListModel> list = new List<BugListModel>();
            //var ent = new BugTrackerDbEntities();
            using (var ent = new BugTrackerDbEntities())
            {
                var query = from bug in ent.Bugs
                            join assignment in ent.BugAssignment on bug.BugId equals assignment.BugId into gj
                            from subpet in gj.DefaultIfEmpty()
                            where bug.Sumary.Contains(searchKeyword)
                            select new
                            {
                                bugId = bug.BugId,
                                severity = bug.Severity.Title,
                                reporter = bug.Login.Username,
                                project = bug.Project.Title,
                                version = bug.Version.Title,
                                platform = bug.Platform.Title,
                                component = bug.Component.Title,
                                status = bug.Status.Title,
                                summary = bug.Sumary,
                                time = bug.StatusId == 1 ? bug.CreatedDateTime : bug.StatusId == 2 ? bug.ConfirmedDateTime : bug.StatusId == 3 ? bug.ConfirmedDateTime : bug.StatusId == 4 ? bug.ReSolvedDateTime : bug.StatusId == 5 ? bug.ReOpenedDateTime : bug.StatusId == 6 ? bug.VerifiedDateTime : bug.StatusId == 7 ? bug.ClosedDateTime : null,
                                AssignedBugId = (subpet == null ? 0 : subpet.BugId)
                            };
                foreach (var item in query)
                {
                    if (!(item.AssignedBugId > 0))
                    {
                        list.Add(new BugListModel
                        {
                            BugId = item.bugId,
                            Reporter = item.reporter,
                            Project = item.platform,
                            Version = item.version,
                            Platform = item.platform,
                            Component = item.component,
                            Severity = item.severity,
                            Status = item.status,
                            Date = item.time,
                            Summary = item.summary
                        });
                    }
                }
            }
            return list;
        }
        //list assigned bugs
        public List<BugListModel> AssignedBugList(string searchKeyword = "")
        {
            List<BugListModel> list = new List<BugListModel>();
            //var ent = new BugTrackerDbEntities();
            using (var ent = new BugTrackerDbEntities())
            {
                var query = from bug in ent.Bugs
                            join assignment in ent.BugAssignment on bug.BugId equals assignment.BugId
                            where bug.Sumary.Contains(searchKeyword)
                            select new
                            {
                                bugId = bug.BugId,
                                severity = bug.Severity.Title,
                                reporter = bug.Login.Username,
                                project = bug.Project.Title,
                                version = bug.Version.Title,
                                platform = bug.Platform.Title,
                                component = bug.Component.Title,
                                status = bug.Status.Title,
                                summary = bug.Sumary,
                                time = bug.StatusId == 1 ? bug.CreatedDateTime : bug.StatusId == 2 ? bug.ConfirmedDateTime : bug.StatusId == 3 ? bug.ConfirmedDateTime : bug.StatusId == 4 ? bug.ReSolvedDateTime : bug.StatusId == 5 ? bug.ReOpenedDateTime : bug.StatusId == 6 ? bug.VerifiedDateTime : bug.StatusId == 7 ? bug.ClosedDateTime : null,
                            };
                foreach (var item in query.Distinct())
                {
                    list.Add(new BugListModel
                    {
                        BugId = item.bugId,
                        Reporter = item.reporter,
                        Project = item.platform,
                        Version = item.version,
                        Platform = item.platform,
                        Component = item.component,
                        Severity = item.severity,
                        Status = item.status,
                        Date = item.time,
                        Summary = item.summary
                    });
                }
            }
            return list;
        }

        // bug assigned to active user
        public List<BugListModel> BugAssignedToMe(string searchKeyword = "")
        {
            List<BugListModel> list = new List<BugListModel>();
            //var ent = new BugTrackerDbEntities();
            using (var ent = new BugTrackerDbEntities())
            {
                //joining bugs and bug assignment table to get required information
                var query = from bug in ent.Bugs
                            join assignment in ent.BugAssignment on bug.BugId equals assignment.BugId
                            where assignment.AssignedToId == ActiveUser.LoginId && bug.Sumary.Contains(searchKeyword)
                            select new
                            {
                                bugId = bug.BugId,
                                severity = bug.Severity.Title,
                                reporter = bug.Login.Username,
                                project = bug.Project.Title,
                                version = bug.Version.Title,
                                platform = bug.Platform.Title,
                                component = bug.Component.Title,
                                status = bug.Status.Title,
                                summary = bug.Sumary,
                                time = bug.StatusId == 1 ? bug.CreatedDateTime : bug.StatusId == 2 ? bug.ConfirmedDateTime : bug.StatusId == 3 ? bug.ConfirmedDateTime : bug.StatusId == 4 ? bug.ReSolvedDateTime : bug.StatusId == 5 ? bug.ReOpenedDateTime : bug.StatusId == 6 ? bug.VerifiedDateTime : bug.StatusId == 7 ? bug.ClosedDateTime : null,
                            };
                foreach (var item in query)
                {
                    list.Add(new BugListModel
                    {
                        BugId = item.bugId,
                        Reporter = item.reporter,
                        Project = item.platform,
                        Version = item.version,
                        Platform = item.platform,
                        Component = item.component,
                        Severity = item.severity,
                        Status = item.status,
                        Date = item.time,
                        Summary = item.summary
                    });
                }
            }
            return list;
        }
        //list of bug assigned by active user
        public List<BugListModel> BugAssignedByMe(string searchKeyword = "")
        {
            List<BugListModel> list = new List<BugListModel>();
            //var ent = new BugTrackerDbEntities();
            using (var ent = new BugTrackerDbEntities())
            {
                var query = from bug in ent.Bugs
                            where bug.ReporterId == ActiveUser.LoginId && bug.Sumary.Contains(searchKeyword)
                            select new
                            {
                                bugId = bug.BugId,
                                severity = bug.Severity.Title,
                                reporter = bug.Login.Username,
                                project = bug.Project.Title,
                                version = bug.Version.Title,
                                platform = bug.Platform.Title,
                                component = bug.Component.Title,
                                status = bug.Status.Title,
                                summary = bug.Sumary,
                                time = bug.StatusId == 1 ? bug.CreatedDateTime : bug.StatusId == 2 ? bug.ConfirmedDateTime : bug.StatusId == 3 ? bug.ConfirmedDateTime : bug.StatusId == 4 ? bug.ReSolvedDateTime : bug.StatusId == 5 ? bug.ReOpenedDateTime : bug.StatusId == 6 ? bug.VerifiedDateTime : bug.StatusId == 7 ? bug.ClosedDateTime : null,
                            };
                foreach (var item in query)
                {
                    list.Add(new BugListModel
                    {
                        BugId = item.bugId,
                        Reporter = item.reporter,
                        Project = item.platform,
                        Version = item.version,
                        Platform = item.platform,
                        Component = item.component,
                        Severity = item.severity,
                        Status = item.status,
                        Date = item.time,
                        Summary = item.summary
                    });
                }
            }
            return list;
        }
        //details of particular bug
        public BugModel BugDetail(int bugId)
        {
            var model = new BugModel();
            using (var ent = new BugTrackerDbEntities())
            {
                var bug = ent.Bugs.FirstOrDefault(x => x.BugId == bugId);
                model.BugId = bug.BugId;
                model.ReporterId = bug.ReporterId;
                model.PlatformId = bug.PlatformId;
                model.ProjectId = bug.ProjectId;
                model.SeverityId = bug.SeverityId;
                model.VersionId = bug.VersionId;
                model.StatusId = bug.StatusId;
                model.ComponentId = bug.ComponentId;
                model.Sumary = bug.Sumary;
                model.AttachmentId = bug.AttachmentId;
                model.AttachmentFileName = bug.Attachment.AttachmentFileName;
                model.AttachementText = bug.Attachment.AttachementText;
                model.Description = bug.Description;
                model.VersionControlIssueId = bug.VersionControlIssueId;
            }
            return model;
        }

        // list of all bugs
        public List<BugListModel> BugList()
        {
            List<BugListModel> list = new List<BugListModel>();
            //var ent = new BugTrackerDbEntities();
            using (var ent = new BugTrackerDbEntities())
            {
                var query = from bug in ent.Bugs
                            select new
                            {
                                bugId = bug.BugId,
                                severity = bug.Severity.Title,
                                reporter = bug.Login.Username,
                                project = bug.Project.Title,
                                version = bug.Version.Title,
                                platform = bug.Platform.Title,
                                component = bug.Component.Title,
                                status = bug.Status.Title,
                                summary = bug.Sumary,
                                time = bug.StatusId == 1 ? bug.CreatedDateTime : bug.StatusId == 2 ? bug.ConfirmedDateTime : bug.StatusId == 3 ? bug.ConfirmedDateTime : bug.StatusId == 4 ? bug.ReSolvedDateTime : bug.StatusId == 5 ? bug.ReOpenedDateTime : bug.StatusId == 6 ? bug.VerifiedDateTime : bug.StatusId == 7 ? bug.ClosedDateTime : null,
                            };
                foreach (var item in query)
                {
                    list.Add(new BugListModel
                    {
                        BugId = item.bugId,
                        Reporter = item.reporter,
                        Project = item.platform,
                        Version = item.version,
                        Platform = item.platform,
                        Component = item.component,
                        Severity = item.severity,
                        Status = item.status,
                        Date = item.time,
                        Summary = item.summary
                    });
                }
            }
            return list;
        }
        //modify a bug
        public bool Edit(BugModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var insertBug = ent.Bugs.Find(model.BugId);
                    insertBug.ProjectId = model.ProjectId;
                    insertBug.PlatformId = model.PlatformId;
                    insertBug.ComponentId = model.ComponentId;
                    insertBug.VersionId = model.VersionId;
                    insertBug.SeverityId = model.SeverityId;
                    insertBug.Sumary = model.Sumary;
                    insertBug.Description = model.Description;
                    insertBug.AttachmentId = model.AttachmentId;
                    ent.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}