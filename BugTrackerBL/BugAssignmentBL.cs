﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BugTrackerBL
{
    //Business logic class for bug assignment
    public class BugAssignmentBL
    {
        //New bug assignment
        public bool Create(BugAssignmentModel model)
        {
            try
            {

                using (var ent = new BugTrackerDbEntities())
                {
                    var exists = ent.BugAssignment.Where(x => x.BugId == model.BugId && x.AssignedToId == model.AssignedToId);
                    if (exists.Any())
                    {
                        return false;
                    }
                    var insertBugAssignment = new BugAssignment
                    {
                        AssignedById = ActiveUser.LoginId,
                        BugId = model.BugId,
                        AssignedToId = model.AssignedToId
                    };
                    ent.BugAssignment.Add(insertBugAssignment);
                    ent.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }
        //modify existing bug assignments
        public bool Edit(BugAssignmentModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var exists = ent.BugAssignment.Where(x => x.BugId == model.BugId && x.AssignedToId == model.AssignedToId);
                    if (exists.Any())
                    {
                        return false;
                    }
                    var BugAssignment = ent.BugAssignment.Find(model.BugAssignmentId);
                    BugAssignment.BugId = model.BugId;
                    BugAssignment.AssignedToId = model.AssignedToId;
                    BugAssignment.AssignedById = ActiveUser.LoginId;
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        // delete a bug assignment
        public bool Delete(int BugAssignmentId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var BugAssignment = ent.BugAssignment.Find(BugAssignmentId);
                    ent.BugAssignment.Remove(BugAssignment);
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //List all bug assignment
        public List<BugAssignmentModel> List()
        {
            List<BugAssignmentModel> list = new List<BugAssignmentModel>();
            var ent = new BugTrackerDbEntities();
            var BugAssignmentList = from a in ent.BugAssignment
                                    join b in ent.Login on a.AssignedToId equals b.LoginId
                                    select new
                                    {
                                        BugAssignmentId=a.BugAssignmentId,
                                        AssignedToId=a.AssignedToId,
                                        BugId=a.BugId,
                                        AssignedById=a.AssignedById,
                                        AssignedTo=b.Username

                                    };
            foreach (var item in BugAssignmentList)
            {
                list.Add(new BugAssignmentModel
                {
                    BugAssignmentId = item.BugAssignmentId,
                    AssignedToId = item.AssignedToId,
                    BugId = item.BugId,
                    AssignedById = item.AssignedById,
                    AssignedTo=item.AssignedTo
                });
            }
            return list;
        }
    }
}
