﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;

namespace BugTrackerBL
{
    //Business logic class for bug image/code attachement
    public class AttachmentBL
    {
        //create new attachment
        public bool Create(AttachmentModel model, out int attachmentId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var insertAttachment = new Attachment
                    {
                        AttachmentFileName = model.AttachmentFileName,
                        AttachementText = model.AttachementText
                    };
                    ent.Attachment.Add(insertAttachment);
                    ent.SaveChanges();
                    attachmentId = insertAttachment.AttachmentId;
                    return true;
                }
            }
            catch (Exception)
            {
                attachmentId = 0;
                return false;
            }

        }
        //edit existing attachment
        public bool Edit(AttachmentModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var attachment = ent.Attachment.Find(model.AttachmentId);
                    attachment.AttachmentFileName = model.AttachmentFileName;
                    attachment.AttachementText = model.AttachementText;
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //delete an attachment
        public bool Delete(int AttachmentId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Attachment = ent.Attachment.Find(AttachmentId);
                    ent.Attachment.Remove(Attachment);
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //list attachments
        public List<AttachmentModel> List()
        {
            List<AttachmentModel> list = new List<AttachmentModel>();
            var ent = new BugTrackerDbEntities();
            var AttachmentList = ent.Attachment;
            foreach (var item in AttachmentList)
            {
                list.Add(new AttachmentModel
                {
                    AttachmentId = item.AttachmentId,
                    AttachmentFileName = item.AttachmentFileName,
                    AttachementText = item.AttachementText
                });
            }
            return list;
        }
        //get image from a attachment
        public byte[] GetImage(int attachmentId)
        {
            byte[] imageData = null;
            using (var dB = new BugTrackerDbEntities())
            {
                imageData = dB.Attachment.Find(attachmentId).AttachmentFileName;
            }
            return imageData;
        }
    }
}
