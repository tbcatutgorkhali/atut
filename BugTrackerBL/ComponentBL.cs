﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;

namespace BugTrackerBL
{
    // Business logic of project componets
    public class ComponentBL
    {

        //create new component
        public bool Create(ComponentModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var insertComponent = new Component
                    {
                        Title = model.Title,
                        Description = model.Description,
                        CreatedBy=model.CreatedBy
                    };
                    ent.Component.Add(insertComponent);
                    ent.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }

        //edit existing component
        public bool Edit(ComponentModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Component = ent.Component.Find(model.ComponentId);
                    Component.Title = model.Title;
                    Component.Description = model.Description;
                    Component.CreatedBy = model.CreatedBy;
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        //delete component
        public bool Delete(int ComponentId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Component = ent.Component.Find(ComponentId);
                    ent.Component.Remove(Component);
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        //list all compoenent
        public List<ComponentModel> List()
        {
            List<ComponentModel> list = new List<ComponentModel>();
            var ent = new BugTrackerDbEntities();
            var ComponentList = ent.Component;
            foreach (var item in ComponentList)
            {
                list.Add(new ComponentModel
                {
                    ComponentId = item.ComponentId,
                    Title = item.Title,
                    Description = item.Description,
                    CreatedBy=item.CreatedBy,
                    CreateByName=item.Login.Username
                });
            }
            return list;
        }
    }
}
