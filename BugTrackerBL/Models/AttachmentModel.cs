﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackerBL.Models
{
    // This is view model class for file/code attachment with bug
    public class AttachmentModel
    {
        public int AttachmentId { get; set; }
        public byte[] AttachmentFileName { get; set; }
        public string AttachementText { get; set; }
    }
}
