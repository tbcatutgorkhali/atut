﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackerBL.Models
{
    //This is business logic model class for bug comments
    public class BugCommentsModel
    {
        public int CommentId { get; set; }
        public int BugId { get; set; }
        public int CommentBy { get; set; }
        public string CommentByName { get; set; }
        public string Comment { get; set; }
        [Display(Name="Comment Date")]
        public DateTime? CommentDate { get; set; }
    }
}
