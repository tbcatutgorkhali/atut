﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackerBL.Models
{

    //This is business logic model class for Bug
    public class BugModel
    {
        public int BugId { get; set; }
        public int ProjectId { get; set; }
        public int ComponentId { get; set; }
        public int VersionId { get; set; }
        public int SeverityId { get; set; }
        public int PlatformId { get; set; }
        public int StatusId { get; set; }
        public string Sumary { get; set; }
        public int? AttachmentId { get; set; }
        public string Description { get; set; }
        public int ReporterId { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public int? ConfirmedBy { get; set; }
        public DateTime? ConfirmedDateTime { get; set; }
        public int? ReOpenedBy { get; set; }
        public DateTime? ReOpenedDateTime { get; set; }
        public int? ReSolvedBy { get; set; }
        public DateTime? ReSolvedDateTime { get; set; }
        public int? VerifiedBy { get; set; }
        public DateTime? VerifiedDateTime { get; set; }
        public int? ClosedBy { get; set; }
        public DateTime? ClosedDateTime { get; set; }
        public byte[] AttachmentFileName { get; set; }
        public string AttachementText { get; set; }
        public int? VersionControlIssueId { get; set; }


    }
    public class BugListModel
    {
        public int BugId { get; set; }
        public string Reporter { get; set; }
        public string Project { get; set; }
        public string Component { get; set; }
        public string Version { get; set; }
        public string Severity { get; set; }
        public string Platform { get; set; }
        public string Status { get; set; }
        public DateTime? Date { get; set; }
        public string Summary { get; set; }
    }
}
