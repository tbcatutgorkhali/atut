﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackerBL.Models
{
    //This is business logic model class for user types
    public class UserTypeModel
    {
        public int UserTypeId { get; set; }
        public string Title { get; set; }
    }
}
