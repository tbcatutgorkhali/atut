﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackerBL.Models
{
    //This is business logic model class for project and user access
    public class ProjecAccessModel
    {
        public int ProjectAccessId { get; set; }
        public int? ProjectId { get; set; }
        public int? UserId { get; set; }
    }
}
