﻿namespace BugTrackerBL.Models
{
    //This is business logic model class for login 
    public class LoginModel
    {
        public int LoginId { get; set; }
        public int UserTypeId { get; set; }
        public string UserTypeTitle { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsLocker { get; set; }
        public string LockStatus { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

}
