﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackerBL.Models
{
    //This is business logic model class for project component
    public class ComponentModel
    {
        public int ComponentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
        public string CreateByName { get; set; }
    }
}
