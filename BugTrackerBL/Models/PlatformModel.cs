﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackerBL.Models
{
    //This is business logic model class for project platform
    public class PlatformModel
    {
        public int PlatformId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
