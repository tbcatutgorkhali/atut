﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackerBL.Models
{

    //This is business logic model class for bug assignment
    public class BugAssignmentModel
    {
        public int BugAssignmentId { get; set; }
        public int BugId { get; set; }
        public int AssignedToId { get; set; }
        public int AssignedById { get; set; }
        public string AssignedTo { get; set; }
    }
}
