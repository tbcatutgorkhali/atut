﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackerBL.Models
{
    //This is a static class that holds information of actively logged in user.
    public static class ActiveUser
    {
        //UserId
        public static int LoginId { get; set; }

        //User Type
        public static int UserTypeId { get; set; }

        //Username
        public static string Username { get; set; }

        //Title of user type
        public static string UserType { get; set; }
    }
}
