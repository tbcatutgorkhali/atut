﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTrackerBL.Models
{
    //This is enum that holds hard coded values of user type and bug status type 
    public enum UserTypeEnum
    {
        Admin = 1,
        Tester = 2,
        QA = 3,
        Specialist = 4,
        Debugger = 5,
    }
    public enum StatusEnum
    {
        UnConfirmed = 1,
        New = 2,
        Assigned = 3,
        Resolved = 4,
        ReOpened = 5,
        Verified = 6,
        Closed = 7
    }
    public enum SeverityEnum
    {
        trivial = 1,
        minor = 2,
        major = 3,
        critical = 4,
        blocker = 5
    }
}
