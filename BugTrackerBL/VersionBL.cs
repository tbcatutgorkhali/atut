﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;

namespace BugTrackerBL
{
    //business logic for project version
    public class VersionBL
    {
        //add version
        public bool Create(VersionModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var insertVersion = new BugTrackerDL.Version
                    {
                        Title = model.Title,
                        Description = model.Description
                    };
                    ent.Version.Add(insertVersion);
                    ent.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }
        //modify version
        public bool Edit(VersionModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Version = ent.Version.Find(model.VersionId);
                    Version.Title = model.Title;
                    Version.Description = model.Description;
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //delete version
        public bool Delete(int VersionId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var Version = ent.Version.Find(VersionId);
                    ent.Version.Remove(Version);
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //list all versions
        public List<VersionModel> List()
        {
            List<VersionModel> list = new List<VersionModel>();
            var ent = new BugTrackerDbEntities();
            var VersionList = ent.Version;
            foreach (var item in VersionList)
            {
                list.Add(new VersionModel
                {
                    VersionId = item.VersionId,
                    Title = item.Title,
                    Description = item.Description
                });
            }
            return list;
        }
    }
}
