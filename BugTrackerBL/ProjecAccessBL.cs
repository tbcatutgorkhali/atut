﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;

namespace BugTrackerBL
{

    //business logic for project assignment
    public class ProjecAccessBL
    {
        //create project assignment
        public bool Create(ProjecAccessModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var insertProjecAccess = new ProjecAccess
                    {
                        UserId = model.UserId,
                        ProjectId = model.ProjectId
                    };
                    ent.ProjecAccess.Add(insertProjecAccess);
                    ent.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }
        //modify existing assignment
        public bool Edit(ProjecAccessModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var ProjecAccess = ent.ProjecAccess.Find(model.ProjectAccessId);
                    ProjecAccess.ProjectId = model.ProjectId;
                    ProjecAccess.UserId = model.UserId;
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //delete an assignemnt
        public bool Delete(int ProjecAccessId)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var ProjecAccess = ent.ProjecAccess.Find(ProjecAccessId);
                    ent.ProjecAccess.Remove(ProjecAccess);
                    ent.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //list all project assignments
        public List<ProjecAccessModel> List()
        {
            List<ProjecAccessModel> list = new List<ProjecAccessModel>();
            var ent = new BugTrackerDbEntities();
            var ProjecAccessList = ent.ProjecAccess;
            foreach (var item in ProjecAccessList)
            {
                list.Add(new ProjecAccessModel
                {
                    ProjectAccessId = item.ProjectAccessId,
                    UserId = item.UserId,
                    ProjectId = item.ProjectId
                });
            }
            return list;
        }
    }
}
