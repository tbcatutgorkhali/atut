﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BugTrackerBL
{
    //Business logic for bug comments
    public class BugCommentsBL
    {
        //create new comment
        public bool Create(BugCommentsModel model)
        {
            try
            {
                using (var ent = new BugTrackerDbEntities())
                {
                    var insertBugComments = new BugComments
                    {
                        CommentBy = ActiveUser.LoginId,
                        BugId = model.BugId,
                        CommentDate = DateTime.Now,
                        Comment = model.Comment
                    };
                    ent.BugComments.Add(insertBugComments);
                    ent.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }
        //unneccessary codes
        //public bool Delete(int BugCommentsId)
        //{
        //    try
        //    {
        //        using (var ent = new BugTrackerDbEntities())
        //        {
        //            var BugComments = ent.BugComments.Find(BugCommentsId);
        //            ent.BugComments.Remove(BugComments);
        //            ent.SaveChanges();
        //        }
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}
        //list all comments
        public List<BugCommentsModel> List(int bugId)
        {
            List<BugCommentsModel> list = new List<BugCommentsModel>();
            var ent = new BugTrackerDbEntities();
            var BugCommentsList = ent.BugComments.Where(x => x.BugId == bugId).OrderByDescending(x => x.CommentDate);
            foreach (var item in BugCommentsList)
            {
                list.Add(new BugCommentsModel
                {
                    CommentId = item.CommentId,
                    CommentBy = item.CommentBy,
                    BugId = item.BugId,
                    CommentByName = item.Login.Username,
                    CommentDate = item.CommentDate,
                    Comment = item.Comment
                });
            }
            return list;
        }
    }
}
