﻿using BugTrackerBL.Models;
using BugTrackerDL;
using System;
using System.Collections.Generic;

namespace BugTrackerBL
{
    //business logic for severity
    public class SeverityBL
    {
        //list all severity types
        public List<SeverityModel> List()
        {
            List<SeverityModel> list = new List<SeverityModel>();
            var ent = new BugTrackerDbEntities();
            var severityList = ent.Severity;
            foreach (var item in severityList)
            {
                list.Add(new SeverityModel
                {
                    SeverityId = item.SeverityId,
                    Title = item.Title,
                    Description = item.Description
                });
            }
            return list;
        }
    }
}
